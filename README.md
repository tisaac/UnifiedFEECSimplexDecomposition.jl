# UnifiedFEECSimplexDecomposition

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://tisaac.gitlab.io/UnifiedFEECSimplexDecomposition.jl/dev)
[![Build Status](https://gitlab.com/tisaac/UnifiedFEECSimplexDecomposition.jl/badges/main/pipeline.svg)](https://gitlab.com/tisaac/UnifiedFEECSimplexDecomposition.jl/pipelines)
[![Coverage](https://gitlab.com/tisaac/UnifiedFEECSimplexDecomposition.jl/badges/main/coverage.svg)](https://gitlab.com/tisaac/UnifiedFEECSimplexDecomposition.jl/commits/main)
[![Coverage](https://codecov.io/gl/tisaac/UnifiedFEECSimplexDecomposition.jl/branch/main/graph/badge.svg)](https://codecov.io/gl/tisaac/UnifiedFEECSimplexDecomposition.jl)
