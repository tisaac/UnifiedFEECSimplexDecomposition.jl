
RM = rm -rf
JULIA = julia
JULIAPROJ = $(JULIA) --project
CODECOV =
JULIATEST = $(JULIAPROJ)
GENHTML = genhtml
BROWSER = firefox
CD = cd
search = 
retest = false

all:

cleancov:
	$(RM) src/*.cov test/*.cov

clean: cleancov
	$(RM) report.info report

test:
	UFSD_TEST_REGEX="$(search)" UFSD_TEST_RETEST="$(retest)" $(JULIATEST) $(CODECOV) test/runtests.jl

report.info: CODECOV = --code-coverage=user

report.info: test
	$(JULIA) -e 'import Pkg; Pkg.add("Coverage"); using Coverage; coverage = process_folder(); LCOV.writefile("report.info", coverage);'

report: clean report.info
	$(GENHTML) -o report report.info

format:
	$(JULIA) -e 'import Pkg; Pkg.add("JuliaFormatter"); using JuliaFormatter; format(".");'

docs:
	$(JULIA) --project=docs -e 'using Pkg; Pkg.develop(PackageSpec(path=pwd())); Pkg.instantiate(); include("docs/make.jl")'

.PHONY: clean test report format docs cleancov

