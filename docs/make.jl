using UnifiedFEECSimplexDecomposition
using Documenter

DocMeta.setdocmeta!(
    UnifiedFEECSimplexDecomposition,
    :DocTestSetup,
    :(using UnifiedFEECSimplexDecomposition);
    recursive = true,
)

makedocs(;
    modules = [UnifiedFEECSimplexDecomposition],
    authors = "Toby Isaac <toby.isaac@gmail.com> and contributors",
    sitename = "UnifiedFEECSimplexDecomposition.jl",
    repo = Remotes.GitLab("tisaac", "UnifiedFEECSimplexDecomposition.jl"),
    format = Documenter.HTML(;
        canonical = "https://tisaac.gitlab.io/UnifiedFEECSimplexDecomposition.jl",
        edit_link = "main",
        assets = String[],
    ),
    pages = [
        "Home" => "index.md",
        "IncreasingMaps" => "increasing_maps.md",
        "AltArrays" => "alt_arrays.md",
        "Simplices" => "simplices.md",
    ],
)
