```@meta
CurrentModule = UnifiedFEECSimplexDecomposition
```

# UnifiedFEECSimplexDecomposition

Documentation for [UnifiedFEECSimplexDecomposition](https://gitlab.com/tisaac/UnifiedFEECSimplexDecomposition.jl).

This package has tools for working with [differential forms](https://en.wikipedia.org/wiki/Differential_form)
on [simplices](https://en.wikipedia.org/wiki/Simplex) in the context of
[finite element exterior calculus](https://en.wikipedia.org/wiki/Finite_element_exterior_calculus) (FEEC).

Our toolbox includes:

* tools from combinatorics for working with increasing maps, which are in the [`.IncreasingMaps`](increasing_maps.md) submodule;

* tools from [exterior algebra](https://en.wikipedia.org/wiki/Exterior_algebra) for working with ``\mathrm{Alt}\,V``, the alternating multilinear forms on ``V``, which are in the [`.AltArrays`](alt_arrays.md) submodule;

- tools from geometry for working with simplices, which are in the [`.Simplices`](simplices.md) submodule;


The two main families of function spaces on simplices used for computation in FEEC are
the ``\mathcal{P}_r \Lambda^k``, the full space of polynomial differential forms,
and ``\mathcal{P}_r^- \Lambda^k``, the trimmed space of polynomial differential forms (for more information
see the [Periodic Table of the Finite Elements](https://www-users.cse.umn.edu/~arnold/femtable/index.html)).

For a function space to be useful for computation in most finite element
software, it must be _geometrically decomposable_ into local basis functions.
By local, we mean that each basis function is associated with a single mesh
point ``p`` (mesh point being a vertex, edge, face, cell, or general
``d``-cell) and is supported only on the cells that contain ``p``.

Letting ``X \in \{ \mathcal{P}_r \Lambda^k, \mathcal{P}_r^- \Lambda^k \}``,
a geometric decomposition of ``X(T) `` for a simplex ``T`` has two steps:

1. Identifying ``\mathring{X}(f)``, the subspace of _trace-free_ functions of
   ``X(f)`` for each mesh subsimplex ``f`` in ``\Delta(T)``, the [simplicial complex](https://en.wikipedia.org/wiki/Simplicial_complex)
   of ``T``.

2. Defining a linear extension operator ``E_f: X(f) \to X(T)`` that lifts
   boundary data from ``f`` into ``T`` in a consistent way for each ``f`` in
   ``\Delta(T)``.

Using these two steps the geometric decomposition of ``X(T)`` is a direct-sum decomposition,

```math
X(T) = \bigoplus_{f\in\Delta(T)} E_f [ \mathring{X}(f) ].
```

## Index

```@index
```

