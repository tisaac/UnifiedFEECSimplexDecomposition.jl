```@meta
CurrentModule = UnifiedFEECSimplexDecomposition.IncreasingMaps
```

# Increasing Maps (submodule `.IncreasingMaps`)

## Index

```@index
Modules = [ IncreasingMaps ]
```

## Reference

```@autodocs
Modules = [ IncreasingMaps ]
```
