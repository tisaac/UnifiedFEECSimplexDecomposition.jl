```@meta
CurrentModule = UnifiedFEECSimplexDecomposition.AltArrays
```

# Arrays of alternating multilinear forms (submodule `.AltArrays`)

## Index

```@index
Modules = [ AltArrays ]
```

## Reference

```@autodocs
Modules = [ AltArrays ]
```
