```@meta
CurrentModule = UnifiedFEECSimplexDecomposition.Simplices
```

# Simplices (submodule `.Simplices`)

## Index

```@index
Modules = [ Simplices ]
```

## Reference

```@autodocs
Modules = [ Simplices ]
```
