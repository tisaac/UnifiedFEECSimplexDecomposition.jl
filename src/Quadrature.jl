
module Quadrature

using ..Polynomials

export simplex_gauss_legendre_quadrature

import LinearAlgebra: SymTridiagonal, eigen

import SpecialFunctions: loggamma

function gauss_jacobi_quadrature(n::Integer, α::Real, β::Real)
    ABC = collect(jacobi_recurrence.(1:n+1, α, β))
    A, B, C = map(x -> getfield.(ABC, x), fieldnames(eltype(ABC)))
    a = -B[1:n] ./ A[1:n]
    b = C[2:n+1] ./ (A[1:n] .* A[2:n+1])
    A = SymTridiagonal(a, sqrt.(b))
    x, V = eigen(A)
    if α == β
        x = (x .- reverse(x)) ./ 2
        if n % 2 != 0
            x[div(n + 1, 2)] = 0.0
        end
    end
    μ₀ = 2^(α + β + 1) * exp(loggamma(α + 1) + loggamma(β + 1) - loggamma(α + β + 2))
    w = V[1, :] .^ 2 * μ₀
    if α == β
        w = (w .+ reverse(w)) ./ 2
    end
    return (x, w)
end

function simplex_gauss_legendre_quadrature(::Type{T}, d::Integer, n::Integer) where {T}
    d >= 0 && n >= 0 || throw(DomainError("dimension $d and degree $n cannot be negative"))
    if d == 0
        return (ones(T, 1, 1), ones(T, 1))
    end
    if d == 1
        x, w = gauss_jacobi_quadrature(n, zero(T), zero(T))
        λ = [(1 .- x) ./ 2;; (1 .+ x) ./ 2]
        w = w ./ √2
        return (λ, w)
    end
    xₜ, wₜ = gauss_jacobi_quadrature(n, d - 1, 0)
    λₜ = (1 .+ xₜ) ./ 2
    λₛ, wₛ = simplex_gauss_legendre_quadrature(T, d - 1, n)
    λ = Array{T}(undef, n^d, d + 1)
    w = kron(wₜ, wₛ) * (√(d + 1) / (2^d * √d))
    for i = 1:d
        λ[:, i] = reshape(stack([λₛ[:, i] .* (1 - l) for l in λₜ]), n^d)
    end
    λ[:, d+1] = kron(λₜ, ones(T, n^(d - 1)))
    return (λ, w)
end

simplex_gauss_legendre_quadrature(d::Integer, n::Integer) =
    simplex_gauss_legendre_quadrature(Float64, d, n)

end
