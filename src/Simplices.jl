
module Simplices

using LinearAlgebra

using ..IncreasingMaps

using ..AltArrays

export Simplex,
    equilateral_simplex,
    right_simplex,
    barycentric_simplex,
    dim,
    vertices,
    tangents,
    volume_form,
    signed_volume,
    barycentric_one_forms,
    dλ

import Subscripts: sub

@doc raw"""
    Simplex{T}(v::AbstractMatrix{T}) where {T<:AbstractFloat}
    Simplex{T}(v; tangents=t)

Datatype for a ``d``-simplex embedded in ``\mathbb{R}^n`` defined by a ``d+1
\times n``-dimensional matrix `v`.

Calculations on the resulting simplex can be done with respect to an intrinsic
``d``-dimensional orthonormal coordinate system or the extrinsic
``n``-dimensional coordinate system that is used to define the coordinates of
the vertices.  If ``d = n``, then these coordinate systems are the same by default.
If ``d < n``, a coordinate system will be computed automatically from vectors that
span the the simplex.  An intrinsic coordinate system can be explicitly specified
with the `tangents` argument, which should be given a ``d \times d``-dimensional
matrix where each row is a tangent vector.

See also: [`equilateral_simplex`](@ref)
"""
struct Simplex{T}
    n::Int64
    d::Int64
    vertices::Matrix{T}
    vertices_intrinsic::Matrix{T}
    tangents::Matrix{T}
    tangents_barycentric::Matrix{T}
    Dλ::Matrix{T}
    Dλ_intrinsic::Matrix{T}
    Dλ_intrinsic_pinv::Matrix{T}
    dλ::AltVector{T,Matrix{T}}
    dλ_intrinsic::AltVector{T,Matrix{T}}
    dλ_barycentric::AltVector{T,Matrix{T}}
    volume_form::Alt{T}
    volume_form_barycentric::Alt{T}
    signed_volume_d_factorial::T
    function Simplex(
        vertices::M;
        tangents = nothing,
        atol = zero(T),
        rtol = atol > 0 ? 0 : size(vertices)[2]^2 * eps(T),
    ) where {T<:AbstractFloat,M<:AbstractMatrix{T}}
        (n_vertices, n) = size(vertices)
        n_vertices > 0 || throw(ArgumentError("Simplex must have at least one vertex"))
        d = n_vertices - 1
        # vectors that span the tangent space
        vectors = vertices[2:end, :] .- vertices[[1], :]
        U, S, V = svd(vectors)
        Vt = V'
        r = rank(Diagonal(S), atol = atol, rtol = rtol)
        r == d || throw(
            ArgumentError(
                "Provided $d-simplex is in an $r-dimensional subspace, degenerate simplices are not supported",
            ),
        )
        # orthonormal basis of the tangent space
        computed_tangents = d == n ? Matrix{T}(I, n, n) : Vt[1:r, :]
        # if the user does not provide tangents and d < n, then by convention
        # the signed volume should be positive
        if d < n && det(vectors * computed_tangents') < 0
            computed_tangents[1, :] *= -1
        end
        if !isnothing(tangents)
            (rₜ, dₜ) = size(tangents)
            (dₜ == n) || throw(
                DimensionMismatch(
                    "tangents are $dₜ-dimensional vectors, expected $n-dimensional vectors matching vertices",
                ),
            )
            (rₜ == d) || throw(
                ArgumentError(
                    "Provided tangents span $rₜ-dimensional subspace, vertices are in $d-dimensional",
                ),
            )
            E₁ = tangents * tangents' - I
            norm(E₁, Inf) <= max(atol, rtol) || throw(
                ArgumentError(
                    "Provided tangents are not orthonormal at error $(norm(E₁,Inf))",
                ),
            )
            E₂ = tangents - (tangents * computed_tangents') * computed_tangents
            norm(E₂, Inf) <= max(atol, rtol) || throw(
                ArgumentError(
                    "Provided tangents do not span computed tangent space at error $(norm(E₂,Inf)), tolerance $(max(atol,rtol))",
                ),
            )
            computed_tangents = Matrix{T}(deepcopy(tangents))
        end
        signed_volume_d_factorial = det(vectors * computed_tangents')
        vertices_intrinsic = vertices * computed_tangents'
        Dλ_intrinsic = [vertices_intrinsic'; ones(T, 1, d + 1)] \ [I; zeros(T, 1, d)]
        tangents_barycentric = deepcopy(Dλ_intrinsic')
        Dλ_intrinsic_pinv = pinv(Dλ_intrinsic)
        dλ_intrinsic = AltArray(1, d, Dλ_intrinsic)
        dλ = (computed_tangents) ⋆ (dλ_intrinsic)
        dλ_barycentric = AltArray(1, d + 1, Matrix{T}(I, d + 1, d + 1) .- one(T) / (d + 1))
        Dλ = copy(array(dλ))
        volume_form = (computed_tangents) ⋆ (vol(T, d))
        volume_form_barycentric = (Dλ_intrinsic_pinv) ⋆ (vol(T, d))
        return new{T}(
            n,
            d,
            Matrix{T}(deepcopy(vertices)),
            vertices_intrinsic,
            computed_tangents,
            tangents_barycentric,
            Dλ,
            Dλ_intrinsic,
            Dλ_intrinsic_pinv,
            dλ,
            dλ_intrinsic,
            dλ_barycentric,
            volume_form,
            volume_form_barycentric,
            signed_volume_d_factorial,
        )
    end
end

@doc raw"""
    equilateral_simplex(d, l)
    equilateral_simplex(d) # edge length 1

Create an equilateral ``d``-simplex with edge length ``l``.
The simplex is positively oriented and its barycenter
is at the origin.

# Example

```jldoctest
julia> equilateral_simplex(1)
1-Simplex in ℝ^1 [Simplex{Float64}]
          x
  v₁   -0.5
  v₂    0.5

julia> equilateral_simplex(3, 6√2)
3-Simplex in ℝ^3 [Simplex{Float64}]
          x      y      z
  v₁   -3.0   -3.0   -3.0
  v₂    5.0   -1.0   -1.0
  v₃   -1.0    5.0   -1.0
  v₄   -1.0   -1.0    5.0
```
"""
function equilateral_simplex(d::Integer, l::T) where {T<:Real}
    x = √T(d + 1)
    V = [-ones(T, 1, d) ./ x; Matrix{T}(I, d, d) .- 1 / (x * (x + 1))]
    V *= l / √T(2)
    return Simplex(V)
end

equilateral_simplex(d::Integer) = equilateral_simplex(d, 1.0)

@doc raw"""
    right_simplex(d, l)
    right_simplex(d) # leg length 1

Create a ``d``-simplex that is made up of points
in the postive orthant of ``\mathbb{R}^d`` whose
coordinates sum to ``l``,

```math
T = \{ x \in \mathbb{R}^d; x_i > 0 \forall i, \sum_i x_i \leq l\}.
```

# Example

```jldoctest
julia> right_simplex(2)
2-Simplex in ℝ^2 [Simplex{Float64}]
         x     y
  v₁   0.0   0.0
  v₂   1.0   0.0
  v₃   0.0   1.0
```
"""
function right_simplex(d::Integer, l::T) where {T<:Real}
    return Simplex([zeros(T, 1, d); l * Matrix{T}(I, d, d)])
end

right_simplex(d::Integer) = right_simplex(d, 1.0)

@doc raw"""
    barycentric_simplex(T, d)
    barycentric_simplex(d)    # = barycentric_simplex(Float64, d)

Create a ``d''-simplex in ``\mathbb{R}^{d+1}`` whose coordinates
are identical with its [barycentric coordinates](https://en.wikipedia.org/wiki/Barycentric_coordinate_system),

```math
T = \{ x \in \mathbb{R}^{d+1}; x_i > 0 \forall i, \sum_i x_i = 1\}.
```

# Example

```jldoctest
julia> barycentric_simplex(0)
0-Simplex in ℝ^1 [Simplex{Float64}]
         x
  v₁   1.0

julia> barycentric_simplex(1)
1-Simplex in ℝ^2 [Simplex{Float64}]
         x     y
  v₁   1.0   0.0
  v₂   0.0   1.0

julia> barycentric_simplex(2)
2-Simplex in ℝ^3 [Simplex{Float64}]
         x     y     z
  v₁   1.0   0.0   0.0
  v₂   0.0   1.0   0.0
  v₃   0.0   0.0   1.0
```
"""
barycentric_simplex(::Type{T}, d::Integer) where {T<:Real} =
    Simplex(Matrix{T}(I, d + 1, d + 1))

barycentric_simplex(d::Integer) = barycentric_simplex(Float64, d)

@doc raw"""
    (d, n) = dim(t::Simplex)

Get a named tuple with the intrinsic dimension `d` of a [`Simplex`](@ref) and
the extrinsic embedding dimnsion `n` of its vertices.
"""
dim(t::Simplex) = (d = t.d, n = t.n)

@doc raw"""
    vertices(t::Simplex, :extrinsic)
    vertices(t::Simplex, :intrinsic)
    vertices(t::Simplex, :barycentric)
    vertices(t::Simplex)               # = vertices(t, :extrinsic)

Get a matrix representing the vertices of a simplex for a coordinate system `c`
(which is `:extrinsic` by default).  If `c == :extrinsic`, the result is a
``d+1 \times n``-dimensional matrix in the coordinates defining the simplex; if
`c == :intrinsic`, the result is a ``d+1 \times d``-dimensional matrix in an
internal coordinate system (which can be controlled with the `tangents`
argument when constructing a [`Simplex`](@ref)); if `c == :barycentric`, the
result is a ``(d + 1)``-dimensional identity matrix, because by definition
vertex ``v_i`` is at ``e_i`` in the barycentric coordinate system.

# Example

```jldoctest
julia> T = Simplex([1.0 2.0 3.0; 3.36 2.48 3.0; 1.48 4.64 3.0])
2-Simplex in ℝ^3 [Simplex{Float64}]
         x     y     z
  v₁   1.0   2.0   3.0
  v₂   3.36  2.48  3.0
  v₃   1.48  4.64  3.0

julia> vertices(T)
3×3 Matrix{Float64}:
 1.0   2.0   3.0
 3.36  2.48  3.0
 1.48  4.64  3.0

julia> vertices(T, :extrinsic)
3×3 Matrix{Float64}:
 1.0   2.0   3.0
 3.36  2.48  3.0
 1.48  4.64  3.0

julia> vertices(T, :intrinsic)
3×2 Matrix{Float64}:
 2.2   0.4
 4.0  -1.2
 4.6   1.6

julia> vertices(T, :barycentric)
3×3 Matrix{Float64}:
 1.0  0.0  0.0
 0.0  1.0  0.0
 0.0  0.0  1.0
```
"""
function vertices(t::Simplex{T}, c) where {T}
    if c == :extrinsic
        return t.vertices
    elseif c == :intrinsic
        return t.vertices_intrinsic
    elseif c == :barycentric
        return Matrix{T}(I, t.d + 1, t.d + 1)
    else
        throw(ArgumentError("Invalid coordinate system $c"))
    end
end

vertices(t) = vertices(t, :extrinsic)

@doc raw"""
    tangents(t::Simplex, :extrinsic)
    tangents(t::Simplex, :intrinsic)
    tangents(t::Simplex, :barycentric)
    tangents(t::Simplex)               # = tangents(t, :extrinsic)

Get a matrix whose rows form a basis for the tangent space of a simplex in
different coordinate systems.  If `:intrinsic`, the tangents 
are by definition the canonical basis, so a ``d``-dimensional identity matrix
is returned.  If `:extrinsic`, returns a ``(d
\times n)``-dimensional matrix whose rows are orthogonal; these
tangents relate the extrinsic and intrinsic coordinates of the vertices by
`vertices(T, :intrinsic) = vertice(T, :extrinsic) * tangents(T, :extrinsic)'`.
If `:barycentric`, the tangents are a ``(d \times d + 1)``-dimensional matrix:
each row is the image of the equivalent intrinsic tangent vector pushed forward
to the barycentric reference triangle.

# Example

```jldoctest
julia> T = Simplex([1.0 2.0 3.0; 3.36 2.48 3.0; 1.48 4.64 3.0])
2-Simplex in ℝ^3 [Simplex{Float64}]
         x     y     z
  v₁   1.0   2.0   3.0
  v₂   3.36  2.48  3.0
  v₃   1.48  4.64  3.0

julia> tangents(T)
2×3 Matrix{Float64}:
  0.6  0.8  -0.0
 -0.8  0.6   0.0

julia> tangents(T, :extrinsic)
2×3 Matrix{Float64}:
  0.6  0.8  -0.0
 -0.8  0.6   0.0

julia> tangents(T, :intrinsic)
2×2 Matrix{Float64}:
 1.0  0.0
 0.0  1.0

julia> tangents(T, :barycentric)
2×3 Matrix{Float64}:
 -0.466667   0.2  0.266667
  0.1       -0.4  0.3
```
"""
function tangents(t::Simplex{T}, c) where {T}
    if c == :extrinsic
        return t.tangents
    elseif c == :intrinsic
        return Matrix{T}(I, t.d, t.d)
    elseif c == :barycentric
        return t.tangents_barycentric
    else
        throw(ArgumentError("Invalid coordinate system $c"))
    end
end

tangents(t) = tangents(t, :extrinsic)

@doc raw"""
    volume_form(t::Simplex)
    volume_form(t::Simplex, :extrinsic)
    volume_form(t::Simplex, :intrinsic)
    volume_form(t::Simplex, :barycentric)

For a ``d``-simplex, get the [volume form](https://en.wikipedia.org/wiki/Volume_form).
Integrating the volume form over the simplex gives the [`signed_volume`](@ref).

# Example

```jldoctest
julia> T = Simplex([8.0 -2.0 -2.0; -2.0 5.0 -4.0; -2.0 -4.0 5.0])
2-Simplex in ℝ^3 [Simplex{Float64}]
          x      y      z
  v₁    8.0   -2.0   -2.0
  v₂   -2.0    5.0   -4.0
  v₃   -2.0   -4.0    5.0

julia> volume_form(T)
Alt^2 ℝ^3 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:      dx ∧ dz:     dy ∧ dz:
 0.666667     -0.666667     0.333333

julia> volume_form(T, :extrinsic)
Alt^2 ℝ^3 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:      dx ∧ dz:     dy ∧ dz:
 0.666667     -0.666667     0.333333

julia> volume_form(T, :intrinsic)
Alt^2 ℝ^2 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:
 1.0

julia> volume_form(T, :barycentric)
Alt^2 ℝ^3 [Alt{Float64, Vector{Float64}}]
    dx ∧ dy:       dx ∧ dz:      dy ∧ dz:
 45.0          -45.0          45.0
```
"""
function volume_form(t::Simplex{T}, c) where {T}
    if c == :extrinsic
        return t.volume_form
    elseif c == :intrinsic
        return vol(T, t.d)
    elseif c == :barycentric
        return t.volume_form_barycentric
    end
end

volume_form(t::Simplex) = volume_form(t, :extrinsic)

@doc raw"""
    signed_volume(t::Simplex)

The volume of a simplex, with a positive sign indicating the simplex is positively oriented with respect
to its [`tangents`](@ref).  Note that tangents are computed automatically for a ``d``-simplex
embedded in an ``n``-dimensional space where ``n > d`` such that the signed volume is positive; 
the [`Simplex`](@ref) must be constructed with an explicitly specified set of [`tangents`](@ref) to
create a negatively oriented simplex in this case.

# Example

```jldoctest
julia> signed_volume(Simplex(Float64[0 0; 1 0; 0 1]))
0.5

julia> signed_volume(Simplex(Float64[0 0; 0 1; 1 0]))
-0.5

julia> signed_volume(Simplex(Float64[0 0 0; 0 1 0; 1 0 0]))
0.5

julia> signed_volume(Simplex(Float64[0 0 0; 0 1 0; 1 0 0], tangents = Float64[1 0 0; 0 1 0]))
-0.5
```
"""
signed_volume(t::Simplex) = t.signed_volume_d_factorial / factorial(t.d)

LinearAlgebra.det(t::Simplex) = t.signed_volume_d_factorial

@doc raw"""
    barycentric_one_forms(T, :extrinsic)
    barycentric_one_forms(T, :intrinsic)
    barycentric_one_forms(T, :barycentric)
    barycentric_one_forms(T)               # = barycentric_one_forms(T, :barycentric)
    dλ(T)                                  # = barycentric_one_forms(T)

Get an [`AltArray`](@ref) containing the 1-forms for the differential of each of the
barycentric coordinates.

# Example

```jldoctest
julia> T = right_simplex(3)
3-Simplex in ℝ^3 [Simplex{Float64}]
         x     y     z
  v₁   0.0   0.0   0.0
  v₂   1.0   0.0   0.0
  v₃   0.0   1.0   0.0
  v₄   0.0   0.0   1.0

julia> dλ(T)
4-element Alt^1 ℝ^3 vector [AltVector{Float64, Matrix{Float64}}]
    dx:      dy:      dz:
 -1.0     -1.0     -1.0
  1.0      0.0      0.0
  0.0      1.0      0.0
  0.0      0.0      1.0
```
"""
function barycentric_one_forms(t::Simplex{T}, c) where {T}
    if c == :extrinsic
        return t.dλ
    elseif c == :intrinsic
        return t.dλ_intrinsic
    elseif c == :barycentric
        return t.dλ_barycentric
    end
end

barycentric_one_forms(t::Simplex) = barycentric_one_forms(t, :extrinsic)

@doc raw"""
    const dλ = barycentric_one_forms

Alias of [`barycentric_one_forms`](@ref).
"""
const dλ = barycentric_one_forms

function Base.summary(io::IO, a::Simplex)
    d, n = dim(a)
    print(io, "$d-Simplex in ℝ^$n [$(typeof(a))]")
end

function Base.show(io::IO, ::MIME"text/plain", a::S) where {S<:Simplex}
    if get(io, :compact, false)::Bool
        print(io, repr(a))
        return
    end
    if get(io, :typeinfo, nothing) != S
        summary(io, a)
    end
    d, n = dim(a)
    if n == 0
        return
    end
    println(io)
    xs = n <= 3 ? ["x" "y" "z"][:, 1:n] : reshape(["x$(sub(string(i)))" for i = 1:n], 1, n)
    vs = reshape(["v$(sub(string(i)))" for i = 1:d+1], d + 1, 1)
    A = [["";;] xs; vs vertices(a)]
    height, width = displaysize(io)
    buffer = IOBuffer()
    Base.print_matrix(
        IOContext(
            buffer,
            :displaysize => (height, width),
            :limit => true,
            :compact => true,
        ),
        A,
    )
    s = String(take!(buffer))
    s = replace(s, "\"" => " ")
    print(io, s)
end

@doc raw"""
    AltArrays.hodge_star(T::Simplex, ω::AbstractAltArray)::AltArray
    ★(T, ω) # [\bigstar] = hodge_star(T, ω)

Given an ``d``-simplex and a ``k``-form ``\omega``, compute an analog of
[`hodge_star`](@ref) that maps ``\omega`` to a ``d - k``-form.  See paper for
details.
"""
function AltArrays.hodge_star(T::Simplex, ω::AbstractAltArray; coordinates = :extrinsic)
    k, n = form_size(ω)
    d, N = dim(T)
    n == N || throw(
        DimensionMismatch("$n-dimensional incompatible with a $N-dimensional simplex"),
    )
    vol = volume_form(T, coordinates)
    r = zeros(Alt(d - k, n), size(ω)...)
    for ρ ∈ Σ(d - k, d + 1)
        dλᵨ = dλ(T, coordinates)^ρ
        r += ((ω ∧ dλᵨ) ⊙ vol) * dλᵨ
    end
    return abs(det(T) / √(d + 1)) * r
end

end
