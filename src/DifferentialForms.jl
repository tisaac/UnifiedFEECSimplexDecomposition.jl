
module DifferentialForms

#using ..IncreasingMaps
#using ..AltArrays
#using ..Simplices
#using ..Quadrature
#
#export AbstractDifferentialForm, domain, is_extrinsic, BarycentricCoordinates, barycentric_coordinates, λ, trace_free_hodge_star, ★̊
#
#@doc raw"""
#    AbstractDifferentialForm{T}
#
#Abstract type for a [differential form](https://en.wikipedia.org/wiki/Differential_form)
#defined on a [`Simplex`](@ref).  An `AbstractDifferentialForm` is callable on a set of
#points, which yields an [`AltArray`](@ref) of algebraic forms at those points.
#
## Methods
#
#- [`domain`](@ref)
#- [`is_extrinsic`](@ref)
#
#`AbstractDifferentialForm` extends the following operations 
#from [`AltArray`](@ref):
#
#- [`form_size`](@ref)
#- [`size`](@ref): the size of the [`AltArray`](@ref) at each evaluation point
#- N-ary wedge product, `a^I`
#"""
#abstract type AbstractDifferentialForm{T} end
#
#struct BarycentricCoordinates{T} <: AbstractDifferentialForm{T}
#    domain::Simplex{T}
#    extrinsic::Bool
#    function BarycentricCoordinates(s::Simplex{T}; extrinsic = true) where {T}
#        return new{T}(s, extrinsic)
#    end
#end
#
#@doc raw"""
#    domain(a::AbstractDifferentialForm)
#
#Returns the [`Simplex`](@ref) on which the differential form is defined.
#"""
#domain(λ::BarycentricCoordinates) = λ.domain
#
#@doc raw"""
#    is_extrinsic(a::AbstractDifferentialForm)
#
#Whether `a` evaluated at a point yields a algebraic form over the
#``n``-dimensional extrinsic vector space where vertices of the simplex are
#defined (`is_extrinsic(a) == true`) or over the intrinsic ``d``-dimensional
#tangent space of the simplex (`is_extrinsic(a) = false`).
#"""
#is_extrinsic(λ::BarcentricCoordinates) = λ.extrinsic
#
#@doc raw"""
#    # barycentric coordinates are considered 0-forms in the extrinsic
#    # coordinate system by default
#    barycentric_coordinates(T)
#
#    # barycentric coordinates are considered 0-forms in the intrinsic
#    # coordinate system if `extrinsic=false`
#    barycentric_coordinates(T; extrinsic=false)
#
#    λ(T) # alias of barycentric_coordinates(T)
#
#Get an [`AbstractDifferentialForm`](@ref)
#for the [barycentric coordinates](https://en.wikipedia.org/wiki/Barycentric_coordinate_system).
#Evaluates to a ``(d + 1)``-dimensional
#[`AltArray`](@ref) at each coordinate.
#"""
#barycentric_coordinates(s::Simplex; extrinsic = true) =
#    BarycentricCoordinates(s; extrinsic = extrinsic)
#
#@doc raw"""
#    const λ = barycentric_coordinates
#
#Alias of [`barycentric_coordinates`](@ref).
#"""
#const λ = barycentric_coordinates
#
#AltArrays.form_size(λ::BarycentricCoordinates) =
#    (0, isextrinsic(λ ? domain(λ).n : domain(λ).d)
#
#function (λ::BarycentricCoordinates{T})(
#    x::AbstractArray;
#    coordinates = :barycentric,
#) where {T}
#    n = λ.domain.n
#    d = λ.domain.d
#    if coordinates == :extrinsic
#        size(x)[end] == n || throw(
#            DimensionMismatch(
#                "Last dimension of coordinates has size $(size(x)[end]), not expected $(n)",
#            ),
#        )
#        x = x * λ.domain.tangents'
#        coordinates = :intrinsic
#    end
#    if coordinates == :intrinsic
#        size(x)[end] == d || throw(
#            DimensionMismatch(
#                "Last dimension of coordinates has size $(size(x)[end]), not expected $(d)",
#            ),
#        )
#        A = [λ.domain.vertices_intrinsic'; ones(T, 1, d + 1)]
#        size_x = size(x)
#        length_x = length(x)
#        n_rows = div(size_x, d)
#        x = reshape(x, n_rows, d)
#        x = [x ones(n_rows, 1)] / A'
#        x = reshape(x, size_x[1:end-1]..., d + 1)
#        coordinates = :barycentric
#    end
#    size(x)[end] == d + 1 || throw(
#        DimensionMismatch(
#            "Last dimension of coordinates has size $(size(x)[end]), not expected $(d + 1)",
#        ),
#    )
#    return AltArray(form_size(λ)..., deepcopy(reshape(x, size(x)..., 1)))
#end
#
#struct DifferentialFormMap{T} <: AbstractDifferentialForm{T}
#    domain::Simplex{T}
#    args::Any
#    f::Any
#    function DifferentialFormMap{T}(s::Simplex{T}, args, f) where {T}
#        return new{T}(args, f)
#    end
#end
#
#function (ast::DifferentialFormMap{T})(
#    x::AbstractArray;
#    coordinates = :barycentric,
#) where {T}
#    args = Tuple(
#        typeof(a) <: AbstractDifferentialForm ? a(x; coordinates = coordinates) : a for
#        a in ast.args
#    )
#    return ast.f(args...)
#end
#
#Base.:^(a::AbstractDifferentialForm{T}, b::AbstractVector{<:Integer}) where {T} =
#    DifferentialFormMap{T}((a, b), Base.:^)
#
#function trace_free_hodge_star(μ::AltArray, λ::AltArray, dλ::AltArray, vol::Alt, volume_d_factorial::Real)
#    k, n = form_size(μ)
#    all(x -> form_size(x)[2] == n || throw(DimensionMismatch("form_size $(form_size(x)) incompatible with given size of μ $(form_size(μ))")), [λ, dλ, vol])
#    d, _ = form_size(vol)
#    T = promote_type(eltype(μ), eltype(λ), eltype(dλ), eltype(vol), typeof(volume_d_factorial))
#    v = zeros(Alt{T}(d - k, n), size(λ)[1:end - 1]...)
#    for ρ in Σ(d - k, n)
#        dλᵨ = dλ^ρ
#        v += vol .⊙ (μ .∧ dλᵨ) .* λ^(ρ') .* dλᵨ
#    end
#    return volume_d_factorial * v
#end
#
#const ★̊ = trace_free_hodge_star
#
#function trace_free_hodge_star(s::Simplex{T}, μ; extrinsic=true) where {T}
#    dλ = extrinsic ? s.dλ : s.dλ_intrinsic
#    λ = barycentric_coordinates(s)
#    vol = extrinsic ? s.volume_form : AltArrays.vol(s.n)
#    volume_d_factorial = abs(s.signed_volume_d_factorial)
#    return DifferentialFormMap{T}((μ, λ), (a, b) -> trace_free_hodge_star(a, b, dλ, vol, volume_d_factorial))
#end
#
#function integrate(a::AbstractDifferentialForm{T}, polynomial_degree) where {T}
#    s = domain(a)
#end
#
#function LinearAlgebra.norm(a::AbstractDifferentialForm{T}, s::Simplex, polynomial_degree) where {T}
#    d = s.d
#    (q, w) = simplex_gauss_legendre_quadrature(T, d, polynomial_degree + 1)
#    w = w .* abs(s.signed_volume_d_factorial) / √(d + 1)
#    a = a(q)
#    z = size(a)
#    wa = w .* a
#    wa = reshape(wa, prod(z)...)
#    a = reshape(a, prod(z)...)
#    v = a' ⊙ wa
#    return sqrt(v)
#end

end
