
module AltArrays

import LinearAlgebra: Adjoint, det, det_bareiss, mul!

_det(a) = det(a)
_det(a::AbstractMatrix{<:Integer}) = det_bareiss(a)

import Subscripts: super, sub

using ..IncreasingMaps

export Alt,
    AltVector,
    AltMatrix,
    AltArray,
    AbstractAlt,
    AbstractAltVector,
    AbstractAltMatrix,
    AbstractAltArray,
    AdjointAltVector,
    AdjointAltMatrix,
    AdjointAltArray,
    array,
    form_size,
    wedge,
    ∧,
    pullback,
    ⋆,
    interior_product,
    ⨼,
    interior_product_right,
    ⨽,
    inner_product,
    ⊙,
    hodge_star,
    ★,
    vol

@doc raw"""
    abstract type AbstractAltArray{T,D} end

Abstract type for a `D`-dimensional array of alternating forms, where `T` is
the numerical type for the field of the vector space that the forms act on.

Both [`AltArray`](@ref) and [`AdjointAltArray`](@ref) are subtypes of `AbstractAltArray`.
Indexing an `AbstractAltArray{T,D}` with an [`IncreasingMap`](@ref) gives
an array that is subtype of `AbstractArray{T,D}`.

See also: [`array`](@ref), [`form_size`](@ref)
"""
abstract type AbstractAltArray{T,D} end

@doc raw"""
    const AbstractAlt{T} = AbstractAltArray{T,0}

An abstract type for a single alternating form.
"""
const AbstractAlt{T} = AbstractAltArray{T,0}

@doc raw"""
    const AbstractAltVector{T} = AbstractAltArray{T,1}

An abstract type for a vector of alternating form.
"""
const AbstractAltVector{T} = AbstractAltArray{T,1}

@doc raw"""
    const AbstractAltMatrix{T} = AbstractAltArray{T,2}

An abstract type for a matrix of alternating form.
"""
const AbstractAltMatrix{T} = AbstractAltArray{T,2}

Base.eltype(a::AbstractAltArray{T,D}) where {T,D} = T
Base.broadcastable(a::AbstractAltArray) = a
Base.ndims(::Type{S}) where {T,D,S<:AbstractAltArray{T,D}} = D
Base.ndims(a::AbstractAltArray) = ndims(typeof(a))
Base.:(==)(a::AbstractAltArray, b::AbstractAltArray) = array(a) == array(b)
Base.:(≈)(a::AbstractAltArray, b::AbstractAltArray; kwargs...) =
    isapprox(array(a), array(b); kwargs...)
array(a::AbstractAltArray) = stack([a[ρ] for ρ ∈ Σ(form_size(a)...)])

@doc raw"""
    form_size(a::AbstractAltArray)::NTuple{2,Int64}
    k, n = form_size(a)

A tuple containing the form degree ``k`` and the dimension ``n``
of the alternating forms in `a`.

# Example

```jldoctest
julia> form_size(zeros(Alt(2,3),4))
(2, 3)
```
"""
form_size(a::AbstractAltArray)::NTuple{2,Int64} =
    throw(MethodError("$(typeof(a)) does not implement `form_size`"))

function Base.summary(io::IO, a::S) where {T,D,S<:AbstractAltArray{T,D}}
    K, N = form_size(a)
    s = size(a)
    d = ndims(S)
    size_string =
        d == 0 ? "" : join(string.(size(a)), "×") * (d == 1 ? "-element" : "") * " "
    field = T <: Complex ? "ℂ" : "ℝ"
    isadjoint = S <: AdjointAltArray && D > 0
    adjoint_string = isadjoint ? "adjoint " : ""
    type_string = (D == 0) ? "" : (D == 1) ? " vector" : (D == 2) ? " matrix" : " array"
    print(
        io,
        "$(size_string)$(adjoint_string)Alt^$K $field^$N$(type_string) [$(typeof(a))]",
    )
end

function Base.show(io::IO, ::MIME"text/plain", a::S) where {T,D,S<:AbstractAltArray{T,D}}
    K, N = form_size(a)
    if get(io, :compact, false)::Bool
        print(io, repr(a))
        return
    end
    if get(io, :typeinfo, nothing) != S
        summary(io, a)
    end
    if binomial(N, K) == 0 || size(a) == (0,)
        return
    end
    println(io)
    if K == 0
        v = a[IncreasingMap(N, Int64[])]
        if typeof(v) <: Number
            print(io, " $v")
        else
            Base.print_matrix(IOContext(io, :compact => true), v)
        end
        return
    end
    s = size(a)
    ndims = length(s)
    NcK = binomial(N, K)
    basis(ρ) =
        (
            N <= 3 ? join(["dx", "dy", "dz"][ρ], " ∧ ") :
            "dx" * join(["$(sub(string(i)))" for i in ρ], ",")
        ) * ":"
    height, width = displaysize(io)
    names = collect(basis.(Σ(K, N)))
    if ndims <= 1 || all(v -> v == 1, s[2:end])
        vs = stack([a[ρ] for ρ ∈ Σ(K, N)])
        l = length(vs)
        named_vs = [reshape(names, 1, NcK); reshape(vs, div(l, NcK), NcK)]
        buffer = IOBuffer()
        Base.print_matrix(
            IOContext(
                buffer,
                :displaysize => (height, width),
                :limit => true,
                :compact => true,
            ),
            named_vs,
        )
        s = String(take!(buffer))
        s = replace(s, "\"" => " ")
        print(io, s)
    elseif all(v -> v == 1, s[1:end-1])
        vs = reduce(vcat, [reshape(a[ρ], 1, s[end]) for ρ ∈ Σ(K, N)])
        named_vs = [names vs]
        buffer = IOBuffer()
        Base.print_matrix(
            IOContext(
                buffer,
                :displaysize => (height, width),
                :limit => true,
                :compact => true,
            ),
            named_vs,
        )
        s = String(take!(buffer))
        s = replace(s, "\"" => " ")
        print(io, s)
    else
        print_newline = false
        print_dots = true
        subheight = div(height, NcK) - 1
        skip = false
        if min(4, s[1]) <= subheight
            skip = false
            subheight = max(8, subheight)
        else
            skip = true
            subheight = max(8, div(height, 4) - 1)
        end
        for ρ in Σ(K, N)
            if index(ρ) <= 2 || index(ρ) == NcK || !skip
                if !print_newline
                    print_newline = true
                else
                    println(io)
                end
                ioctx = IOContext(io, :displaysize => (subheight, width), :compact => true)
                if ndims == 2
                    Base.print_matrix(ioctx, a[ρ], " $(basis(ρ)) ")
                else
                    println(io, " $(basis(ρ))")
                    Base.print_array(ioctx, a[ρ])
                end
                if index(ρ) ≠ NcK
                    println(io)
                end
            else
                if print_dots
                    println(io)
                    println(io, " ⋮")
                    print_dots = false
                end
            end
        end
    end
end

struct AltConstructor end

@doc raw"""
    AltArray{T,D,A} <: AbstractAltArray{T,D}
    AltArray{T,D}(k, n, v::AbstractMatrix{T,D+1}) # wrap a D+1-dimensional array of numbers
    AltArray(k, n, v::AbstractMatrix{T,D})        # = AltArray{D-1}(k, n, a)

Type for a `D+1`-dimensional array `v` that represents a `D`-dimensional array
of algebraic ``k``-forms.  The last dimension of `v` indexes the canonical
basis of ``k``-forms.

See also: [`Alt`](@ref), [`AltVector`](@ref), [`AltMatrix`](@ref)

# Example

```jldoctest
julia> v = reshape(collect(1:6), 1, 2, 3)
1×2×3 Array{Int64, 3}:
[:, :, 1] =
 1  2

[:, :, 2] =
 3  4

[:, :, 3] =
 5  6

julia> a = AltArray(2, 3, v)
1×2 Alt^2 ℝ^3 matrix [AltMatrix{Int64, Array{Int64, 3}}]
  dx ∧ dy:   1  2
  dx ∧ dz:   3  4
  dy ∧ dz:   5  6

julia> ρ = collect(Σ(2, 3))
3-element Vector{IncreasingMap{Int64, Vector{Int64}}}:
 [1, 2]
 [1, 3]
 [2, 3]

julia> a[ρ[1]]
1×2 Matrix{Int64}:
 1  2

julia> a[ρ[2]]
1×2 Matrix{Int64}:
 3  4

julia> a[ρ[3]]
1×2 Matrix{Int64}:
 5  6

```
"""
struct AltArray{T,D,A} <: AbstractAltArray{T,D}
    k::Int64
    n::Int64
    v::A
    function AltArray{T,0}(k, n) where {T<:Number}
        new{T,0,AltConstructor}(k, n, AltConstructor())
    end
    function AltArray(k, n, v::A) where {T<:Number,D,A<:AbstractArray{T,D}}
        n = Int8(n)
        k = Int8(k)
        NcK = binomial(n, k)
        size(v)[end] == NcK || throw(
            DimensionMismatch(
                "Last dimension of array must have length $NcK, not $(size(v)[end])",
            ),
        )
        return new{T,D - 1,A}(k, n, v)
    end
end

form_size(a::AltArray)::NTuple{2,Int64} = (a.k, a.n)

@doc raw"""
    const Alt{T,A} = AltArray{T,0,A}
    Alt(k, n, v::AbstractVector{T})  # construct from a vector

Alias for a 0-dimensional [`AltArray`](@ref): a single alternating form.
"""
const Alt{T,A} = AltArray{T,0,A}

(Alt)(k, n) = Alt{Float64}(k, n)
(Alt)(k, n, v) = AltArray(k, n, v)

@doc raw"""
    const AltVector{T,A} = AltArray{T,1,A}
    AltVector(k, n, v::AbstractMatrix{T})  # construct from a matrix

Alias for a 1-dimensional [`AltArray`](@ref): a vector of alternating form.
"""
const AltVector{T,A} = AltArray{T,1,A}

(AltVector)(k, n, v) = AltArray(k, n, v)

@doc raw"""
    const AltMatrix{T,A} = AltArray{T,2,A}
    AltVector(k, n, v::AbstractArray{T,3}) # construct from a 3d array

Alias for a 2-dimensional [`AltArray`](@ref): a matrix of alternating form.
"""
const AltMatrix{T,A} = AltArray{T,2,A}

(AltMatrix)(k, n, v) = AltArray(k, n, v)

function _index(a::AltArray, i::IncreasingMap)
    K, N = form_size(a)
    (i.n == N && i.k == K) ||
        throw(DomainError("1:$(i.k) → 1:$(i.n) cannot index $(N)D $K-form"))
    return index(i)
end

Base.getindex(a::AltArray, I...) = AltArray(form_size(a)..., getindex(a.v, I..., :))
Base.selectdim(a::AltArray, d::Integer, i) = AltArray(form_size(a)..., selectdim(a.v, d, i))

Base.getindex(a::Alt, i::IncreasingMap) = getindex(a.v, _index(a, i))
Base.getindex(a::AltVector, i::IncreasingMap) = getindex(a.v, :, _index(a, i))
Base.getindex(a::AltMatrix, i::IncreasingMap) = getindex(a.v, :, :, _index(a, i))
Base.getindex(a::AltArray{T,D}, i::IncreasingMap) where {T,D} =
    getindex(a.v, repeat([:], D)..., _index(a, i))

Base.setindex!(a::AltArray, v::AbstractAltArray, I...) = setindex!(a.v, array(v), I..., :)

Base.setindex!(a::Alt, v, i::IncreasingMap) = setindex!(a.v, v, _index(a, i))
Base.setindex!(a::AltVector, v, i::IncreasingMap) = setindex!(a.v, v, :, _index(a, i))
Base.setindex!(a::AltMatrix, v, i::IncreasingMap) = setindex!(a.v, v, :, :, _index(a, i))
Base.setindex!(a::AltArray{T,D}, v, i::IncreasingMap) where {T,D} =
    setindex!(a.v, v, repeat([:], D)..., _index(a, i))

Base.view(a::AltArray, I...) = AltArray(form_size(a)..., view(a.v, I..., :))
Base.maybeview(a::AltArray, I...) = view(a, I...)

Base.view(a::Alt, i::IncreasingMap) = view(a.v, _index(a, i))
Base.view(a::AltVector, i::IncreasingMap) = view(a.v, :, _index(a, i))
Base.view(a::AltMatrix, i::IncreasingMap) = view(a.v, :, :, _index(a, i))
Base.view(a::AltArray{T,D}, i::IncreasingMap) where {T,D} =
    view(a.v, repeat([:], D)..., _index(a, i))
Base.maybeview(a::AltArray, i::IncreasingMap) = view(a, i)
Base.size(a::AltArray) = size(a.v)[1:end-1]
Base.size(a::Alt{T,S}) where {T,S<:AltConstructor} = (0,)

@doc raw"""
    array(a::AbstractAltArray{T,D})

Get an `AbstractArray{T,D+1}` that contains the raw values of an [`AbstractAltArray`](@ref).

# Example

```jldoctest
julia> v = randn(3, binomial(4,2)); a = AltArray(2, 4, v); array(a) == v
true

```
"""
array(a::AltArray) = a.v

Base.zeros(s::Alt{T,AltConstructor}, I::Union{Integer,AbstractUnitRange}...) where {T} =
    AltArray(form_size(s)..., zeros(T, I..., binomial(form_size(s)[[2, 1]]...)))
Base.ones(s::Alt{T,AltConstructor}, I::Union{Integer,AbstractUnitRange}...) where {T} =
    AltArray(form_size(s)..., ones(T, I..., binomial(form_size(s)[[2, 1]]...)))
Base.rand(s::Alt{T,AltConstructor}, I::Integer...) where {T} =
    AltArray(form_size(s)..., rand(T, I..., binomial(form_size(s)[[2, 1]]...)))
Base.randn(s::Alt{T,AltConstructor}, I::Integer...) where {T} =
    AltArray(form_size(s)..., randn(T, I..., binomial(form_size(s)[[2, 1]]...)))

Base.reshape(s::AltArray, I...) =
    AltArray(form_size(s)..., reshape(array(s), I..., binomial(form_size(s)[[2, 1]]...)))

@doc raw"""
    AdjointAltArray{T,D,A} <: AbstractAltArray{T,D}

Symbolic adjoint of an [`AltArray`](@ref)`{T,D,A}`, which is defined
for `D` = 0, 1, and 2.

# Example

```jldoctest
julia> a = im * AltArray(1, 2, [1.0, 2.0])
Alt^1 ℂ^2 [Alt{ComplexF64, Vector{ComplexF64}}]
      dx:        dy:
 0.0+1.0im  0.0+2.0im

julia> a'
Alt^1 ℂ^2 [Alt{ComplexF64, Vector{ComplexF64}}]
      dx:        dy:
 0.0-1.0im  0.0-2.0im

julia> b = im * AltArray(1, 2, reshape(1:4,2,2))
2-element Alt^1 ℂ^2 vector [AltVector{Complex{Int64}, Matrix{Complex{Int64}}}]
    dx:      dy:
 0+1im    0+3im
 0+2im    0+4im

julia> b'
1×2 adjoint Alt^1 ℂ^2 vector [AdjointAltVector{Complex{Int64}, Matrix{Complex{Int64}}}]
  dx:   0-1im  0-2im
  dy:   0-3im  0-4im

julia> c = im * AltArray(1, 2, reshape(1:8,2,2,2))
2×2 Alt^1 ℂ^2 matrix [AltMatrix{Complex{Int64}, Array{Complex{Int64}, 3}}]
 dx: 0+1im  0+3im
     0+2im  0+4im

 dy: 0+5im  0+7im
     0+6im  0+8im

julia> c'
2×2 adjoint Alt^1 ℂ^2 matrix [AdjointAltMatrix{Complex{Int64}, Array{Complex{Int64}, 3}}]
 dx: 0-1im  0-2im
     0-3im  0-4im

 dy: 0-5im  0-6im
     0-7im  0-8im
```
"""
struct AdjointAltArray{T,D,A} <: AbstractAltArray{T,D}
    parent::AltArray{T,D,A}
    function AdjointAltArray(v::AltArray{T,D,A}) where {T,D,A}
        return new{T,D,A}(v)
    end
end

@doc raw"""
    const AdjointAltVector{T,A} = AdjointAltArray{T,1,A}

Alias for the adjoint of an [`AltVector`](@ref).

Note that, as with the adjoints of standard julia vectors,
if `size(a) = (s,)`, then `size(a') = (1,s)`

# Example

```jldoctest
julia> typeof(adjoint(ones(Alt(2,3), 2)))
AdjointAltVector{Float64, Matrix{Float64}} (alias for AdjointAltArray{Float64, 1, Array{Float64, 2}})
```
"""
const AdjointAltVector{T,A} = AdjointAltArray{T,1,A}

@doc raw"""
    const AdjointAltMatrix{T,A} = AdjointAltArray{T,2,A}

Alias for the adjoint of an [`AltMatrix`](@ref).

# Example

```jldoctest
julia> typeof(adjoint(ones(Alt(2,3), 2, 2)))
AdjointAltMatrix{Float64, Array{Float64, 3}} (alias for AdjointAltArray{Float64, 2, Array{Float64, 3}})
```
"""
const AdjointAltMatrix{T,A} = AdjointAltArray{T,2,A}

form_size(a::AdjointAltArray)::NTuple{2,Int64} = form_size(a.parent)

function form_size(a::AbstractAltArray, b::AbstractAltArray)::NTuple{2,Int64}
    form_size(a) == form_size(b) || throw(
        DimensionMismatch("Mismatched form sizes, $(form_size(a)) and $(form_size(b))"),
    )
    return form_size(a)
end

Base.getindex(a::AdjointAltArray, i::IncreasingMap) = getindex(a.parent, i)'
Base.setindex!(a::AdjointAltArray, v, i::IncreasingMap) = setindex!(a.parent, v', i)
Base.view(a::AdjointAltArray, i::IncreasingMap) = view(a.parent, i)'
Base.maybeview(a::AdjointAltArray, i::IncreasingMap) = view(a, i)

Base.size(a::AdjointAltVector) = (1, size(a.parent)...)
Base.size(a::AdjointAltMatrix) = size(a.parent)[[2, 1]]

Base.ndims(::Type{S}) where {S<:AdjointAltVector} = 2

# Adjoint of a scalar k-form is just conj applied to it
Base.adjoint(a::Alt) = Alt(form_size(a)..., conj.(a.v))
Base.adjoint(a::AltVector) = AdjointAltArray(a)
Base.adjoint(a::AltMatrix) = AdjointAltArray(a)

Base.adjoint(a::AdjointAltArray) = a.parent

Base.:*(a::Number, b::AltArray) = AltArray(form_size(b)..., a * b.v)
Base.:*(a::AltArray, b::Number) = AltArray(form_size(a)..., a.v * b)
Base.:*(a::Number, b::AdjointAltArray) = (b.parent * a')'
Base.:*(a::AdjointAltArray, b::Number) = (b' * a.parent)'

Base.:+(a::AbstractAltArray, b::AbstractAltArray) =
    AltArray(form_size(a, b)..., array(a) + array(b))
Base.:-(a::AbstractAltArray, b::AbstractAltArray) =
    AltArray(form_size(a, b)..., array(a) - array(b))
Base.:+(a::AdjointAltArray, b::AdjointAltArray) = (a.parent + b.parent)'
Base.:-(a::AdjointAltArray, b::AdjointAltArray) = (a.parent - b.parent)'

@doc raw"""
    function (a::AltArray}(v::AbstractMatrix)

Apply each ``k``-form form in an [`AltArray`](@ref) to the column vectors of a matrix.
Returns an array of scalars with the same shape as `a`.

# Example

```jldoctest
julia> a = AltVector(2, 3, [1 2 3; 4 5 6])
2-element Alt^2 ℝ^3 vector [AltVector{Int64, Matrix{Int64}}]
   dx ∧ dy:     dx ∧ dz:     dy ∧ dz:
 1            2            3
 4            5            6

julia> v = [1 2; 3 4; 5 6]
3×2 Matrix{Int64}:
 1  2
 3  4
 5  6

julia> a(v)
2-element Vector{Int64}:
 -16
 -40

julia> a(v[:,[2,1]])
2-element Vector{Int64}:
 16
 40
```
"""
function (a::AltArray)(v::AbstractMatrix{<:Number})
    K, N = form_size(a)
    n, k = size(v)
    (n == N && k == K) || throw(DimensionMismatch("size $(size(v)) ≠ ($N,$K)"))
    z = zeros(promote_type(eltype(a), eltype(v)), size(a)...)
    @views for ρ ∈ Σ(K, N)
        z .+= a[ρ] * _det(v[ρ, :])
    end
    return z
end

function _product_size(s₁::Tuple, s₂::Tuple, bcast)
    l₁ = length(s₁)
    l₂ = length(s₂)
    if bcast
        l = max(l₁, l₂)
        S₁ = (s₁..., repeat([1], l - l₁)...)
        S₂ = (s₂..., repeat([1], l - l₂)...)
        return max.(S₁, S₂)
    else
        if l₁ > 0 && l₂ > 0
            if l₁ == 1
                s₁ = (s₁..., 1)
                l₁ = 2
            end
            return (s₁[1:end-1]..., s₂[2:end]...)
        else
            return (s₁..., s₂...)
        end
    end
end

product_size(a, b, bcast) = _product_size(size(a), size(b), bcast)

function product_size(
    a::Union{AdjointAltVector,Adjoint{<:Number,<:AbstractVector}},
    b::Union{AltVector,AbstractVector},
    bcast,
)
    if bcast
        return _product_size(size(a), size(b), bcast)
    else
        return ()
    end
end

function _mul_add!(c, α, a, b)::Nothing
    mul!(c, a, b, α, 1.0)
    return nothing
end
function _mul_add_bcast!(c, α, a, b)::Nothing
    broadcast!((_c, _a, _b) -> _c + α * _a * _b, c, c, a, b)
    return nothing
end
function _mul_add!(z, α, x, y, bcast)::Nothing
    bcast ? _mul_add_bcast!(z, α, x, y) : _mul_add!(z, α, x, y)
    return nothing
end
function _mul_add!(z::AbstractArray{T,0}, α, x, y, bcast)::Nothing where {T}
    z[] = z[] + α * (bcast ? x .* y : x * y)
    return nothing
end

_self(c) = c
_self(c::AbstractArray{T,0}) where {T} = c[]

mul_add!(z, α, x, y, bcast) = _mul_add!(z, α, _self(x), _self(y), bcast)
mul_add!(z, α, x) = mul_add!(z, α, x, 1, false)

function _wedge(a::AbstractAltArray, b::AbstractAltArray, bcast; swap = false)
    J, N = form_size(a)
    K, N2 = form_size(b)
    N == N2 || throw(
        DimensionMismatch("Forms are for vectors of different dimensions, $N and $N2"),
    )
    ptype = promote_type(eltype(a), eltype(b))
    v = zeros(Alt{ptype}(J + K, N), product_size(a, b, bcast)...)
    premult = swap ? (-1)^(J * K) : 1
    @views for ρ ∈ Σ(J + K, N)
        vᵨ = v[ρ]
        for σ ∈ Σ(J, J + K)
            mul_add!(vᵨ, premult * sign(σ), a[ρ[σ]], b[ρ[σ']], bcast)
        end
    end
    return v
end

@doc raw"""
    wedge(a::AbstractAltArray, b::AbstractAltArray)::AbstractAltArray
    ∧(a,b) # the same as wedge(a,b)
    a ∧ b  # the same as wedge(a,b), contracts dimensions like products of matrices and vectors
    a .∧ b # elementwise product

[Exterior (wedge) product](https://en.wikipedia.org/wiki/Exterior_algebra) of alternating forms.
When `a` and `b` are of type [`AbstractAltVector`](@ref) or [`AbstractAltMatrix`](@ref) then
`a ∧ b` contracts a dimension the same as in the products of matrices and vectors in julia.
For instance if `a` and `b` are both matrices of alternating forms, then
``(a \wedge b)_{ij} = \sum_k a_{ik} \wedge b_{kj}.``  For the element wise exterior product of arrays, use
`a .∧ b`.

# Example

```jldoctest
julia> a = AltVector(2, 3, [1 2 3; 4 5 6])
2-element Alt^2 ℝ^3 vector [AltVector{Int64, Matrix{Int64}}]
   dx ∧ dy:     dx ∧ dz:     dy ∧ dz:
 1            2            3
 4            5            6

julia> b = AltVector(1, 3, [7 8 9; 10 11 12])
2-element Alt^1 ℝ^3 vector [AltVector{Int64, Matrix{Int64}}]
    dx:      dy:      dz:
  7        8        9
 10       11       12

julia> a .∧ b
2-element Alt^3 ℝ^3 vector [AltVector{Int64, Matrix{Int64}}]
    dx ∧ dy ∧ dz:
 14
 53

julia> a' ∧ b
Alt^3 ℝ^3 [Alt{Int64, Vector{Int64}}]
    dx ∧ dy ∧ dz:
 67

julia> a ∧ b'
2×2 Alt^3 ℝ^3 matrix [AltMatrix{Int64, Array{Int64, 3}}]
 dx ∧ dy ∧ dz: 14  20
               38  53
```
"""
wedge(a::AbstractAltArray, b::AbstractAltArray; bcast = false) = _wedge(a, b, bcast)
wedge(a::AdjointAltVector, b::Alt; bcast = false) = _wedge(b', a', bcast; swap = true)'
wedge(a::AdjointAltMatrix, b::Alt; bcast = false) = _wedge(b', a', bcast; swap = true)'
wedge(a::Alt, b::AdjointAltVector; bcast = false) = _wedge(b', a', bcast; swap = true)'
wedge(a::Alt, b::AdjointAltMatrix; bcast = false) = _wedge(b', a', bcast; swap = true)'
wedge(a::AdjointAltVector, b::AbstractAltMatrix; bcast = false) =
    _wedge(b', a', bcast; swap = true)'

@doc raw"""
    const ∧ = wedge # \wedge

Alias of [`wedge`](@ref).
"""
const ∧ = wedge

Base.broadcasted(bs::S, ::typeof(∧), a, b) where {S<:Base.Broadcast.BroadcastStyle} =
    ∧(a, b; bcast = true)

# multiplication with a vector or a matrix
function _mul(a::AbstractVecOrMat{<:Number}, b::AbstractAltArray, bcast)
    K, N = form_size(b)
    ptype = promote_type(eltype(a), eltype(b))
    psize = product_size(a, b, bcast)
    v = zeros(Alt{ptype}(K, N), psize...)
    @views for ρ in Σ(K, N)
        mul_add!(v[ρ], 1.0, a, b[ρ], bcast)
    end
    return v
end

# multiplication with a vector or a matrix
function _mul(a::AbstractAltArray, b::AbstractVecOrMat{<:Number}, bcast)
    K, N = form_size(a)
    ptype = promote_type(eltype(a), eltype(b))
    psize = product_size(a, b, bcast)
    v = zeros(Alt{ptype}(K, N), psize...)
    @views for ρ in Σ(K, N)
        mul_add!(v[ρ], 1.0, a[ρ], b, bcast)
    end
    return v
end

# multiplication with 0-form
function _mul(a::AbstractAltArray, b::AbstractAltArray, bcast)
    j, n = form_size(a)
    k, m = form_size(b)
    m == n ||
        throw(DimensionMismatch("Cannot multiply forms acting on different vector spaces"))
    j == 0 ||
        k == 0 ||
        throw(
            DimensionMismatch(
                "Scalar multiplication only defined when operand is a zero form",
            ),
        )
    if j == 0
        a = reshape(array(a), size(a)...)
        return bcast ? a .* b : a * b
    end
    b = reshape(array(b), size(b)...)
    return bcast ? a .* b : a * b
end

Base.:*(a::AbstractAltArray, b::AbstractAltArray; bcast = false) = _mul(a, b, bcast)
Base.:*(a::AbstractAltArray, b::AbstractVecOrMat{<:Number}; bcast = false) =
    _mul(a, b, bcast)
Base.:*(a::AbstractVecOrMat{<:Number}, b::AbstractAltArray; bcast = false) =
    _mul(a, b, bcast)
Base.:*(a::Adjoint{<:Number,<:AbstractVector}, b::Alt; bcast = false) = _mul(b', a', bcast)'
Base.:*(a::Adjoint{<:Number,<:AbstractMatrix}, b::Alt; bcast = false) = _mul(b', a', bcast)'
Base.:*(a::Alt, b::Adjoint{<:Number,<:AbstractVector}; bcast = false) = _mul(b', a', bcast)'
Base.:*(a::Alt, b::Adjoint{<:Number,<:AbstractMatrix}; bcast = false) = _mul(b', a', bcast)'
Base.:*(a::AdjointAltVector, b::AbstractMatrix{<:Number}; bcast = false) =
    _mul(b', a', bcast)'
Base.:*(a::Adjoint{<:Number,<:AbstractVector}, b::AbstractAltMatrix; bcast = false) =
    _mul(b', a', bcast)'

Base.broadcasted(
    bs::S,
    ::typeof(*),
    a::AbstractAltArray,
    b::AbstractAltArray,
) where {S<:Base.Broadcast.BroadcastStyle} = Base.:*(a, b, bcast = true)
Base.broadcasted(
    bs::S,
    ::typeof(*),
    a::AbstractAltArray,
    b::AbstractVecOrMat{<:Number},
) where {S<:Base.Broadcast.BroadcastStyle} = Base.:*(a, b, bcast = true)
Base.broadcasted(
    bs::S,
    ::typeof(*),
    a::AbstractVecOrMat{<:Number},
    b::AbstractAltArray,
) where {S<:Base.Broadcast.BroadcastStyle} = Base.:*(a, b, bcast = true)
Base.broadcasted(
    bs::S,
    ::typeof(*),
    a::Number,
    b::AbstractAltArray,
) where {S<:Base.Broadcast.BroadcastStyle} = a * b
Base.broadcasted(
    bs::S,
    ::typeof(*),
    a::AbstractAltArray,
    b::Number,
) where {S<:Base.Broadcast.BroadcastStyle} = a * b

function _pullback(J::AbstractMatrix{<:Number}, b::AbstractAltArray)
    K, M = form_size(b)
    (m, N) = size(J)
    m == M || throw(DimensionMismatch("Matrix column size $m ≠ $M"))
    ptype = promote_type(eltype(J), eltype(b))
    v = zeros(Alt{ptype}(K, N), size(b)...)
    @views for ρ ∈ Σ(K, N)
        vᵨ = v[ρ]
        for σ ∈ Σ(K, M)
            mul_add!(vᵨ, det(J[σ, ρ]), b[σ])
        end
    end
    return v
end

@doc raw"""
    pullback(J::AbstractMatrix{<:Number}, b::AbstractAltArray)::AbstractAltArray
    J⋆b # ≡ pullback(J,b)

Compute the [pullback](https://en.wikipedia.org/wiki/Pullback_(differential_geometry)#Pullback_of_multilinear_forms)
on each form in an [`AbstractAltArray`](@ref).

# Example

```jldoctest
julia> a = ones(Alt(2,2))
Alt^2 ℝ^2 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:
 1.0

julia> J = [1.0 2.0 3.0; 0.0 4.0 5.0]
2×3 Matrix{Float64}:
 1.0  2.0  3.0
 0.0  4.0  5.0

julia> J⋆a
Alt^2 ℝ^3 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:     dx ∧ dz:      dy ∧ dz:
 4.0          5.0          -2.0
```
"""
pullback(J::AbstractMatrix{<:Number}, b::AbstractAltArray) = _pullback(J, b)

@doc raw"""
    const ⋆ = pullback # \star

Alias of [`pullback`](@ref).
"""
const ⋆ = pullback

# interior product
function _interior_product(a::AbstractAltArray, b::AbstractArray{<:Number}, bcast, swap)
    K, N = form_size(a)
    d = ndims(b)
    size(b)[d] == N || throw(DimensionMismatch("Vector length $(size(b)[d]) ≠ $N"))
    ptype = promote_type(eltype(a), eltype(b))
    b₁ = N > 0 ? selectdim(b, d, 1) : zeros(size(b)[1:end-1]...)
    psize = swap ? product_size(b₁, a, bcast) : product_size(a, b₁, bcast)
    v = zeros(Alt{ptype}(K - 1, N), psize...)
    @views for ρ ∈ Σ(K, N)
        aᵨ = a[ρ]
        for i ∈ Σ(1, K)
            if swap
                mul_add!(v[ρ[i']], sign(i), selectdim(b, d, ρ[i[1]]), aᵨ, bcast)
            else
                mul_add!(v[ρ[i']], sign(i), aᵨ, selectdim(b, d, ρ[i[1]]), bcast)
            end
        end
    end
    return v
end

@doc raw"""
    interior_product(a::AbstractAltArray, b::AbstractArray{<:Number})::AbstractAltArray
    ⨼(a, b) # = interior_product(a, b)
    a ⨼ b   # = interior_product(a, b)
    a .⨼ b  # = interior_product(a, b), elementwise

Compute the [interior product](https://en.wikipedia.org/wiki/Interior_product) between a vector `b` and
each alternating form in an [`AbstractAltArray`](@ref)

If `b` is a multidimensional array, then the last dimension of `b` should represent the components of the
vector space that the alternating forms act on, and the other dimensions of `b` will contract with `a`
like the products of vectors and matrices, following the same pattern as [`wedge`](@ref).

# Example

```jldoctest
julia> a = ones(Alt(1,3), 2, 2)
2×2 Alt^1 ℝ^3 matrix [AltMatrix{Float64, Array{Float64, 3}}]
 dx: 1.0  1.0
     1.0  1.0

 dy: 1.0  1.0
     1.0  1.0

 dz: 1.0  1.0
     1.0  1.0

julia> a ⨼ ones(3) # one vector applied to each form
2×2 Alt^0 ℝ^3 matrix [AltMatrix{Float64, Array{Float64, 3}}]
 3.0  3.0
 3.0  3.0

julia> a ⨼ ones(2,3) # form matrix ⨼ vector of vectors = form vector
2-element Alt^0 ℝ^3 vector [AltVector{Float64, Matrix{Float64}}]
 6.0
 6.0

julia> a ⨼ ones(2,2,3) # form matrix ⨼ matrix of vectors = form matrix
2×2 Alt^0 ℝ^3 matrix [AltMatrix{Float64, Array{Float64, 3}}]
 6.0  6.0
 6.0  6.0
```
"""
interior_product(a::AbstractAltArray, b::AbstractArray{<:Number}; bcast = false) =
    _interior_product(a, b, bcast, false)

@doc raw"""
    interior_product_right(a::AbstractArray{<:Number}, b::AbstractAltArray{<:Number})::AbstractAltArray
    ⨽(a, b) # = interior_product_right(a, b)
    a ⨽ b   # = interior_product_right(a, b)
    a .⨽ b  # = interior_product_right(a, b), elementwise

The same as [`interior_product`](@ref), but with the form-type argument on the right
and the vector-type argument on the left.

# Example
"""
interior_product_right(a::AbstractArray{<:Number}, b::AbstractAltArray; bcast = false) =
    _interior_product(b, a, bcast, true)
interior_product(a::AdjointAltVector, b::AbstractVector{<:Number}; bcast = false) =
    _interior_product(a', conj.(b), bcast, true)'
interior_product(a::AdjointAltMatrix, b::AbstractVector{<:Number}; bcast = false) =
    _interior_product(a', conj.(b), bcast, true)'
interior_product_right(a::AbstractVector{<:Number}, b::AdjointAltVector; bcast = false) =
    _interior_product(b', conj.(a), bcast, false)'
interior_product_right(a::AbstractVector{<:Number}, b::AdjointAltMatrix; bcast = false) =
    _interior_product(b', conj.(a), bcast, false)'
interior_product(a::AdjointAltVector, b::AbstractArray{<:Number,3}; bcast = false) =
    _interior_product(a', permutedims(conj.(b), (2, 1, 3)), bcast, true)'

@doc raw"""
    const ⨼ = interior_product # \intprod

Alias for [`interior_product`](@ref).
"""
const ⨼ = interior_product

Base.broadcasted(bs::S, ::typeof(⨼), a, b) where {S<:Base.Broadcast.BroadcastStyle} =
    ⨼(a, b; bcast = true)

@doc raw"""
    const ⨽ = interior_product_right # \intprodr

Alias for [`interior_product_right`](@ref).
"""
const ⨽ = interior_product_right

Base.broadcasted(bs::S, ::typeof(⨽), a, b) where {S<:Base.Broadcast.BroadcastStyle} =
    ⨽(a, b; bcast = true)

# Inner product
function _inner_product(a::AbstractAltArray, b::AbstractAltArray, bcast)
    K, N = form_size(a)
    K2, N2 = form_size(b)
    (K, N) == (K2, N2) || throw(
        DimensionMismatch(
            "Cannot take inner product of $N-dimensional $K-form and $N2-dimensional $K2-form",
        ),
    )
    ptype = promote_type(eltype(a), eltype(b))
    psize = product_size(a, b, bcast)
    v = zeros(ptype, psize...)
    @views for ρ in Σ(K, N)
        mul_add!(v, 1.0, a[ρ], b[ρ], bcast)
    end
    return psize == () ? v[] : v
end

@doc raw"""
    inner_product(a::AbstractAltArray, b::AbstractAltArray)::AbstractArray{<:Number}
    ⊙(a, b) # = inner_product(a, b)
    a ⊙ b   # = inner_product(a, b), contracting dimensions like matrix-vector products
    a .⊙ b  # elementwise inner product

Compute the inner product between the forms in two [`AbstractAltArray`](@ref)s.  Follows
the same multiplication and broadcast rules as [`wedge`](@ref) and [`interior_product`](@ref).

This introduces a new method instead of extending `LinearAlgebra.dot` because the first argument
is not conjugated for this inner product.

# Example

```jldoctest
julia> a = Alt(2, 3, [im 2*im 3*im; 4*im 5*im 6*im])
2-element Alt^2 ℂ^3 vector [AltVector{Complex{Int64}, Matrix{Complex{Int64}}}]
    dx ∧ dy:      dx ∧ dz:      dy ∧ dz:
 0+1im         0+2im         0+3im
 0+4im         0+5im         0+6im

julia> a .⊙ a
2-element Vector{Complex{Int64}}:
 -14 + 0im
 -77 + 0im

julia> a' ⊙ a
91 + 0im
```
"""
inner_product(a::AbstractAltArray, b::AbstractAltArray; bcast = false) =
    _inner_product(a, b, bcast)
inner_product(a::AdjointAltVector, b::Alt; bcast = false) = _inner_product(b', a', bcast)'
inner_product(a::AdjointAltMatrix, b::Alt; bcast = false) = _inner_product(b', a', bcast)'
inner_product(a::Alt, b::AdjointAltVector; bcast = false) = _inner_product(b', a', bcast)'
inner_product(a::Alt, b::AdjointAltMatrix; bcast = false) = _inner_product(b', a', bcast)'
inner_product(a::AdjointAltVector, b::AbstractAltMatrix; bcast = false) =
    _inner_product(b', a', bcast)'

@doc raw"""
    const ⊙ = inner_product # \odot

Alias of [`inner_product`](@ref).
"""
const ⊙ = inner_product

Base.broadcasted(bs::S, ::typeof(⊙), a, b) where {S<:Base.Broadcast.BroadcastStyle} =
    ⊙(a, b; bcast = true)

# Hodge star
@doc raw"""
    hodge_star(a::AbstractAltArray)::AbstractAltArray
    ★(a) # = hodge_star(a)

Apply the [Hodge star operator](https://en.wikipedia.org/wiki/Hodge_star_operator) to each form in an
[`AbstractAltArray`](@ref).

# Example

```jldoctest
julia> ★(Alt(1, 3, [1.0, 2.0, 3.0]))
Alt^2 ℝ^3 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:      dx ∧ dz:     dy ∧ dz:
 3.0          -2.0          1.0
```
"""
function hodge_star(a::AbstractAltArray)
    K, N = form_size(a)
    v = zeros(Alt{eltype(a)}(N - K, N), size(a)...)
    @views for ρ ∈ Σ(K, N)
        v[ρ'] = sign(ρ) * _self(a[ρ])
    end
    return v
end

@doc raw"""
    const ★ = hodge_star # \bigstar

Alias of [`hodge_star`](@ref).
"""
const ★ = hodge_star

@doc raw"""
    vol(T, n)::Alt{T}
    vol(n) # = vol(Float64, n)

Get the [`volume form`](https://en.wikipedia.org/wiki/Volume_form) for a given vector dimension ``n``.

# Example

```jldoctest
julia> vol(0)
Alt^0 ℝ^0 [Alt{Float64, Vector{Float64}}]
 1.0

julia> vol(1)
Alt^1 ℝ^1 [Alt{Float64, Vector{Float64}}]
   dx:
 1.0

julia> vol(2)
Alt^2 ℝ^2 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy:
 1.0

julia> vol(3)
Alt^3 ℝ^3 [Alt{Float64, Vector{Float64}}]
   dx ∧ dy ∧ dz:
 1.0

julia> vol(ComplexF64, 3)
Alt^3 ℂ^3 [Alt{ComplexF64, Vector{ComplexF64}}]
      dx ∧ dy ∧ dz:
 1.0+0.0im
```
"""
vol(::Type{T}, n) where {T} = ones(Alt{T}(n, n))
vol(n) = vol(Float64, n)

@doc raw"""
    Base.:^(a::AbstractAltArray{T,D}, b::AbstractVector{<:Integer}; dim=D) where {T,D}
    a ^ b # = Base.:^(a, b)

Compute n-ary wedge product (see [`wedge`](@ref)) of selected forms from an [`AbstractAltArray`](@ref).
If `a` is an [`AbstractAltVector`](@ref), for instance, then `a ^ b` is the form

```math
\bigwedge_{i \in b} a_i.
```

If `a` is a ``d``-dimensional array, the result will be a $d-1$-dimensional array.  By default, `b`
selects indices along the last dimension, but this can be changed with the kwarg `dim`.
"""
function Base.:^(
    a::AbstractAltArray{T,D},
    b::AbstractVector{<:Integer};
    dim = D,
) where {T,D}
    k, n = form_size(a)
    dim ∈ 1:D || throw(BoundsError("Cannot index dimension $dim of a $D-dimensional array"))
    m = size(a)[dim]
    all(x -> x ∈ 1:m, b) || throw(BoundsError("Index set $b ⊈ 1:$m"))
    r = ones(Alt(0, n), size(a)[1:dim-1]..., size(a)[dim+1:D]...)
    for i in b
        r = r .∧ selectdim(a, dim, i)
    end
    return r
end

end
