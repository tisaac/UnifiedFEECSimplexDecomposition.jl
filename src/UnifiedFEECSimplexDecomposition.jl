module UnifiedFEECSimplexDecomposition

include("IncreasingMaps.jl")

using .IncreasingMaps
export IncreasingMap, Σ, index, increasing_maps

include("AltArrays.jl")

using .AltArrays
export Alt,
    AltVector,
    AltMatrix,
    AltArray,
    AbstractAlt,
    AbstractAltVector,
    AbstractAltMatrix,
    AbstractAltArray,
    AdjointAltVector,
    AdjointAltMatrix,
    AdjointAltArray,
    array,
    form_size,
    wedge,
    ∧,
    pullback,
    ⋆,
    interior_product,
    ⨼,
    interior_product_right,
    ⨽,
    inner_product,
    ⊙,
    hodge_star,
    ★,
    vol

include("Simplices.jl")

using .Simplices
export Simplex,
    equilateral_simplex,
    right_simplex,
    barycentric_simplex,
    dim,
    vertices,
    tangents,
    volume_form,
    signed_volume,
    barycentric_one_forms,
    dλ

include("Polynomials.jl")

include("Quadrature.jl")

include("DifferentialForms.jl")

using .DifferentialForms

export BarycentricCoordinates, barycentric_coordinates, trace_free_hodge_star, ★̊

end
