
module Polynomials

export jacobi_recurrence, jacobi_norm_squared, pkd

import SpecialFunctions: loggamma

import Combinatorics: multiexponents

raw"""
    jacobi_recurrence(n, α, β)

Get the recurrence coefficients ``(a_n, b_n, c_n) for the ``n``th ``(α, β)`` Jacobi polynomials,

```math
P^{(α, β)}_n(x) = (a_n x + b_n) P^{(α, β)}_{n-1}(x) - c_n P^{(α, β)}_{n-2}(x).
```
"""
function jacobi_recurrence(n::Integer, α::A, β::B) where {A<:Real,B<:Real}
    n >= 1 || throw(
        DomainError("Jacobi polynomial recurrence coefficients are not defined for n = $n"),
    )
    T = promote_type(A, B)
    T = typeof(one(T) / one(T))
    if (n == 1)
        return ((α + β + 2) / 2, (α - β) / 2, zero(T))
    else
        denom = 2n * (n + α + β) * (2n + α + β - 2)
        a = (2n + α + β - 1) * (2n + α + β) * (2n + α + β - 2) / denom
        b = (2n + α + β - 1) * (α^2 - β^2) / denom
        c = 2(n + α - 1) * (n + β - 1) * (2n + α + β) / denom
        return (a, b, c)
    end
end

raw"""
The `k`the entry is ``y^{k-1} P^{(\alpha, \beta)}_{k-1} (x / y)``.
"""
function warped_jacobi(
    n::Integer,
    α::A,
    β::B,
    x::AbstractArray{<:Real},
    y::AbstractArray{<:Real},
) where {A<:Real,B<:Real}
    n >= 0 || throw(DomainError("P^($α, $β)_n not defined for n = $n"))
    T = eltype(x)
    Q = zeros(T, size(x)..., n + 1)
    P = eachslice(Q; dims = ndims(x) + 1)
    P[1] .= one(T)
    y² = y .* y
    if n > 0
        a, b, _ = jacobi_recurrence(1, α, β)
        copy!(P[2], a .* x .+ b .* y)
    end
    for k = 2:n
        Pₖ₋₂ = P[k-1]
        Pₖ₋₁ = P[k]
        Pₖ = P[k+1]
        a, b, c = jacobi_recurrence(k, α, β)
        copy!(Pₖ, (a * x .+ b * y) .* Pₖ₋₁ - (c * y²) .* Pₖ₋₂)
    end
    return Q
end

raw"""
The integral ``\int_{-1}^1 (1 - x)^\alpha (1 + x)^\beta (P^{(\alpha, \beta)})_n(x)^2 dx.``
"""
function jacobi_norm_squared(n::Integer, α::A, β::B) where {A<:Real,B<:Real}
    n >= 0 || throw(DomainError("P^($α, $β)_n not defined for n = $n"))
    if n == 0
        return 2^(α + β + 1) * exp(loggamma(α + 1) + loggamma(β + 1) - loggamma(α + β + 2))
    else
        return 2^(α + β + 1) / (2n + α + β + 1) * exp(
            loggamma(n + α + 1) + loggamma(n + β + 1) - loggamma(n + α + β + 1) -
            loggamma(n + 1),
        )
    end
end


raw"""
`warped_jacobi` for a set of polynomial degrees, normalized by weighted L^2 norm
"""
function normalized_warped_jacobi(
    degrees::AbstractArray{<:Integer},
    α::A,
    β::B,
    x::AbstractArray{<:Real},
    y::AbstractArray{<:Real},
) where {A<:Real,B<:Real}
    n = maximum(degrees)
    n_map = [x + 1 for x in degrees]
    P = warped_jacobi(n, α, β, x, y)
    for (k, p) in zip(0:n, eachslice(P, dims = ndims(P)))
        p ./= √(jacobi_norm_squared(k, α, β))
    end
    if n_map == 1:n+1
        return P
    else
        P = stack([selectdim(P, ndims(P), k) for k in n_map])
        return P
    end
end

raw"""
sort and compress a list, and give scatter indices for where the original indices are in the output
"""
function unique_and_scatter(s)
    p = sortperm(s)
    ip = invperm(p)
    sorted_s = s[p]
    M = length(sorted_s)
    u = M == 0 ? [] : [1; filter(i -> sorted_s[i-1] != sorted_s[i], 2:M); M + 1]
    K = length(u)
    filtered_sorted_s = sorted_s[u[1:K-1]]
    c = collect(
        Iterators.flatten(fill(i[1], i[2]) for i in enumerate(u[2:end] .- u[1:end-1])),
    )
    ip = c[ip]
    @assert all(filtered_sorted_s[ip] .== s)
    return (filtered_sorted_s, ip)
end

function warped_pkd(
    indices::AbstractArray{NTuple{D,S}},
    λ::AbstractArray{<:Real},
    y::AbstractArray{<:Real},
) where {D,S<:Integer}
    size(λ)[end] == D + 1 || throw(
        DimensionMismatch(
            "Multindex length $D, but barycentric coordinate length $(length(λ))",
        ),
    )
    λ_trans = selectdim(λ, ndims(λ), D + 1)
    λ_sub = selectdim(λ, ndims(λ), 1:D)
    D >= 1 || throw(DomainError("The last dimension of λ must have at size > 1."))
    T = eltype(λ)
    two = T(2)
    if D == 1
        factor = two^(T(1 / 4))
        return normalized_warped_jacobi(
            map(x -> x[1], indices),
            0,
            0,
            2 * λ_trans .- y,
            y,
        ) * factor
    else
        subindices = map(i -> i[1:end-1], indices)
        transverse_indices = collect(zip(sum.(subindices), map(i -> i[end], indices)))

        unique_subindices, ip = unique_and_scatter(subindices)
        P_sub = warped_pkd(unique_subindices, λ_sub, y .- λ_trans)
        P = Array(selectdim(P_sub, ndims(P_sub), ip))

        unique_transverse, ip = unique_and_scatter(transverse_indices)
        M = length(unique_transverse)
        u =
            M == 0 ? [] :
            [
                1
                filter(i -> unique_transverse[i-1][1] != unique_transverse[i][1], 2:M)
                M + 1
            ]
        sets = collect(zip(u[1:end-1], u[2:end] .- 1))
        subsums = map(i -> unique_transverse[i[1]][1], sets)
        subdegrees = map(i -> map(x -> x[2], unique_transverse[i[1]:i[2]]), sets)
        Q_sub = similar(P, size(P)[1:end-1]..., 0)
        for (sum, degrees) in zip(subsums, subdegrees)
            factor = two^(T(sum + D / 2)) * (T(D) / T(D + 1))^(T(1 / 4))
            Q_this =
                normalized_warped_jacobi(degrees, 2 * sum + D - 1, 0, 2 * λ_trans .- y, y) *
                factor
            Q_sub = cat(Q_sub, Q_this; dims = ndims(Q_sub))
        end
        Q = Array(selectdim(Q_sub, ndims(Q_sub), ip))
        return P .* Q
    end
end

function pkd(n::Integer, λ::AbstractArray{<:Real})
    d = size(λ)[end] - 1
    d >= 0 || throw(DomainError("The last dimension of λ must be non-empty."))
    if d == 0
        return ones(eltype(λ), size(λ)[1:end-1]...)
    else
        indices = collect(Tuple(v for v in x[2:end]) for x in (multiexponents(d + 1, n)))
        y = ones(eltype(λ), size(λ)[1:end-1]...)
        return warped_pkd(indices, λ, y)
    end
end

end
