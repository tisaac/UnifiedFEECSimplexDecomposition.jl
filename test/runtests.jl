using UnifiedFEECSimplexDecomposition
using UnifiedFEECSimplexDecomposition.Polynomials
using UnifiedFEECSimplexDecomposition.Quadrature
using Test
import Combinatorics: permutations, levicivita, integer_partitions
import LinearAlgebra: I, Diagonal, Bidiagonal, qr, det, cond, norm

function get_show(io, a, args...)
    ioctx = IOContext(io, :displaysize => (30, 80), :limit => true, args...)
    show(ioctx, "text/plain", a)
    return String(take!(io))
end

function get_summary(io, a, args...)
    ioctx = IOContext(io, :displaysize => (30, 80), :limit => true, args...)
    summary(ioctx, a)
    return String(take!(io))
end

function get_random_square_matrix(n, cond)
    d = rand() * 1.0 ./ collect(range(1, n == 1 ? 1 : cond, n))
    U = qr(randn(n, n)).Q
    V = qr(randn(n, n)).Q
    return U * Diagonal(d) * V'
end

function test_increasing_map()
    @test eltype(IncreasingMap(Int8(3), Int8(5), collect(2:4), true)) == Int64
    for n ∈ 0:6
        for k ∈ 0:n
            for (i, ρ) ∈ enumerate(Σ(k, n))
                @test allunique(ρ)
                @test issorted(ρ)
                @test index(ρ) == i
                @test sign(ρ) ∈ [1, -1]
                @test sign(ρ') == (-1)^(k * (n - k)) * sign(ρ)
                @test issetequal(ρ ∪ ρ', 1:n)
                @test issetequal(ρ ∩ ρ', [])
                @test sign(ρ) == levicivita([ρ; ρ'])
                for j ∈ 0:k
                    for σ ∈ Σ(k, j)
                        @test typeof(ρ[σ]) <: IncreasingMap
                        @test Base.maybeview(ρ, σ) == ρ[σ]
                    end
                end
            end
        end
    end
    @test_throws DomainError IncreasingMap(3, 2:4)
end

function test_increasing_map_summary()
    io = IOBuffer()
    a = IncreasingMap(5, 2:4)
    @test get_summary(io, a) == "1:3 → 1:5 IncreasingMap{Int64, Vector{Int64}}"
end

function test_alt_array_constructors()
    @test size(Alt(2, 3)) == (0,)
    @test zeros(Alt(2, 3)) == Alt(2, 3, zeros(3))
    @test zeros(Alt(2, 3), 4) == AltVector(2, 3, zeros(4, 3))
    @test zeros(Alt(2, 3), 4, 3) == AltMatrix(2, 3, zeros(4, 3, 3))
    @test zeros(Alt(2, 3), 4, 3, 2) == AltArray(2, 3, zeros(4, 3, 2, 3))
    @test zeros(Alt{ComplexF32}(2, 3), 4, 3, 2) ==
          AltArray(2, 3, zeros(ComplexF32, 4, 3, 2, 3))
    @test ones(Alt(2, 3)) == Alt(2, 3, ones(3))
    @test ones(Alt(2, 3), 4) == AltVector(2, 3, ones(4, 3))
    @test ones(Alt(2, 3), 4, 3) == AltMatrix(2, 3, ones(4, 3, 3))
    @test ones(Alt(2, 3), 4, 3, 2) == AltArray(2, 3, ones(4, 3, 2, 3))
    @test ones(Alt{UInt8}(2, 3), 4, 3, 2) == AltArray(2, 3, ones(UInt8, 4, 3, 2, 3))
    @test size(array(rand(Alt(2, 3)))) == (3,)
    @test size(array(rand(Alt(2, 3), 4))) == (4, 3)
    @test size(array(rand(Alt(2, 3), 4, 3))) == (4, 3, 3)
    @test size(array(rand(Alt(2, 3), 4, 3, 2))) == (4, 3, 2, 3)
    @test eltype(array(rand(Alt{Int16}(2, 3), 1))) == Int16
    @test size(array(randn(Alt(2, 3)))) == (3,)
    @test size(array(randn(Alt(2, 3), 4))) == (4, 3)
    @test size(array(randn(Alt(2, 3), 4, 3))) == (4, 3, 3)
    @test size(array(randn(Alt(2, 3), 4, 3, 2))) == (4, 3, 2, 3)
    @test eltype(array(randn(Alt{Float32}(2, 3), 1))) == Float32
    a = zeros(Alt(2, 3), 3, 3, 3)
    for ρ ∈ Σ(2, 3)
        a[ρ] = ones(3, 3, 3)
    end
    @test a == ones(Alt(2, 3), 3, 3, 3)
    b = randn(2, 6, 3)
    @test Alt(2, 3, reshape(b, 4, 1, 3, 3)) == reshape(Alt(2, 3, b), 4, 1, 3)
end

function test_alt_array_slicing()
    for n = 0:4
        for k = 0:n
            for d = 0:3
                a = randn(collect(2:d+1)..., binomial(n, k))
                slices = (1:2, 2:3, 2:3)
                dims = (2, 2, 2)
                b = a[slices[1:d]..., :]
                a_form = Alt(k, n, a)
                b_form = Alt(k, n, b)
                @test a_form[slices[1:d]...] == b_form
                @views begin
                    @test a_form[slices[1:d]...] == b_form
                end
                for d2 = 1:d
                    c = selectdim(a, d2, [1])
                    c_form = Alt(k, n, c)
                    @test selectdim(a_form, d2, [1]) == c_form
                end
                a_form[slices[1:d]...] = zeros(Alt(k, n), dims[1:d]...)
                @test all(a[slices[1:d]..., :] .== 0.0)
            end
        end
    end
end

function test_alt_array_show()
    io = IOBuffer()
    a = zeros(Alt(-1, 3))
    @test get_show(io, a, :compact => true) == repr(a)
    @test get_show(io, a) == get_summary(io, a)
    a = zeros(Alt(0, 3))
    @test get_show(io, a) == """
Alt^0 ℝ^3 [Alt{Float64, Vector{Float64}}]
 0.0"""
    a = zeros(Alt(0, 3), 2)
    @test get_show(io, a) == """
2-element Alt^0 ℝ^3 vector [AltVector{Float64, Matrix{Float64}}]
 0.0
 0.0"""
    a = zeros(Alt(1, 3), 2)
    @test get_show(io, a) == """
2-element Alt^1 ℝ^3 vector [AltVector{Float64, Matrix{Float64}}]
   dx:     dy:     dz: 
 0.0     0.0     0.0
 0.0     0.0     0.0"""
    a = zeros(Alt(1, 3), 1, 2)
    @test get_show(io, a) == """
1×2 Alt^1 ℝ^3 matrix [AltMatrix{Float64, Array{Float64, 3}}]
  dx:   0.0  0.0
  dy:   0.0  0.0
  dz:   0.0  0.0"""
    a = zeros(Alt(1, 3), 2, 2)
    @test get_show(io, a) == """
2×2 Alt^1 ℝ^3 matrix [AltMatrix{Float64, Array{Float64, 3}}]
 dx: 0.0  0.0
     0.0  0.0

 dy: 0.0  0.0
     0.0  0.0

 dz: 0.0  0.0
     0.0  0.0"""
    a = zeros(Alt(1, 3), 2, 2, 2)
    @test get_show(io, a) == """
2×2×2 Alt^1 ℝ^3 array [AltArray{Float64, 3, Array{Float64, 4}}]
 dx:
[:, :, 1] =
 0.0  0.0
 0.0  0.0

[:, :, 2] =
 0.0  0.0
 0.0  0.0

 dy:
[:, :, 1] =
 0.0  0.0
 0.0  0.0

[:, :, 2] =
 0.0  0.0
 0.0  0.0

 dz:
[:, :, 1] =
 0.0  0.0
 0.0  0.0

[:, :, 2] =
 0.0  0.0
 0.0  0.0"""
    a = zeros(Alt(2, 6), 2, 2)
    @test get_show(io, a) == """
2×2 Alt^2 ℝ^6 matrix [AltMatrix{Float64, Array{Float64, 3}}]
 dx₁,₂: 0.0  0.0
        0.0  0.0

 dx₁,₃: 0.0  0.0
        0.0  0.0

 ⋮

 dx₅,₆: 0.0  0.0
        0.0  0.0"""
end

function test_alt_array_adjoint()
    s = IOBuffer()
    a₀ = randn(Alt{ComplexF64}(2, 3))
    summary(s, a₀)
    s₀ = String(take!(s))
    @test s₀ == "Alt^2 ℂ^3 [Alt{ComplexF64, Vector{ComplexF64}}]"
    @test ndims(a₀) == 0
    b₀ = a₀'
    summary(s, b₀)
    t₀ = String(take!(s))
    @test t₀ == "Alt^2 ℂ^3 [Alt{ComplexF64, Vector{ComplexF64}}]"
    @test ndims(b₀) == 0
    c₀ = 1.0 * im
    a₁ = randn(Alt{ComplexF64}(2, 3), 2)
    summary(s, a₁)
    s₁ = String(take!(s))
    @test s₁ == "2-element Alt^2 ℂ^3 vector [AltVector{ComplexF64, Matrix{ComplexF64}}]"
    @test ndims(a₁) == 1
    b₁ = a₁'
    summary(s, b₁)
    t₁ = String(take!(s))
    @test t₁ ==
          "1×2 adjoint Alt^2 ℂ^3 vector [AdjointAltVector{ComplexF64, Matrix{ComplexF64}}]"
    @test ndims(b₁) == 2
    c₁ = [1.0 * im, 2.0 * im]
    a₂ = randn(Alt{ComplexF64}(2, 3), 2, 2)
    summary(s, a₂)
    s₂ = String(take!(s))
    @test s₂ == "2×2 Alt^2 ℂ^3 matrix [AltMatrix{ComplexF64, Array{ComplexF64, 3}}]"
    @test ndims(a₂) == 2
    b₂ = a₂'
    summary(s, b₂)
    t₂ = String(take!(s))
    @test t₂ ==
          "2×2 adjoint Alt^2 ℂ^3 matrix [AdjointAltMatrix{ComplexF64, Array{ComplexF64, 3}}]"
    @test ndims(b₂) == 2
    c₂ = [
        1.0*im 2.0*im
        3.0*im 4.0*im
    ]

    A = [a₀, a₁, a₂]
    B = [b₀, b₁, b₂]
    C = [c₀, c₁, c₂]
    for (a, b, c) in zip(A, B, C)
        @test b' == a
        @test eltype(a) == eltype(array(a))
        @test eltype(b) == eltype(array(b))
        av = array(a)
        bv = array(b)
        av = reshape(av, repeat([1], 3 - ndims(av))..., size(av)...)
        bv = reshape(bv, repeat([1], 3 - ndims(bv))..., size(bv)...)
        @test bv[:] == conj.(permutedims(av, (2, 1, 3)))[:]
        for ρ ∈ Σ(2, 3)
            a[ρ] = c
            if (ndims(a) > 0)
                @test b[ρ] == c'
            end
            b[ρ] = 2.0 * conj.(c')
            if (ndims(a) > 0)
                @test a[ρ] == 2.0 * conj.(c)
            end
            av = view(a, ρ)
            copyto!(av, c)
            @test a[ρ] == c
            if (ndims(a) > 0)
                @test b[ρ] == c'
            end
            if ndims(a[ρ]) > 0
                bv = view(b, ρ)
                copyto!(bv, 2.0 * conj.(c'))
                @test b[ρ] == 2.0 * conj.(c')
                @test a[ρ] == 2.0 * conj.(c)
            end
        end
    end
end

function test_alt_array_linear_algebra()
    for n = 0:4
        for k = 0:n
            for d = 0:3
                v = randn(ComplexF64, repeat([2], d)..., binomial(n, k))
                a = AltArray(k, n, v)
                b = randn(Alt{ComplexF64}(k, n), repeat([2], d)...)
                s = randn(ComplexF64)
                @test √2 .* a == √2 * a
                @test a .* √2 == a * √2
                @test array(s * a) == s * v
                @test array(a * s) == v * s
                @test 0 * a == zeros(Alt{ComplexF64}(k, n), repeat([2], d)...)
                @test s * (a + b) ≈ s * a + s * b
                if d < 3
                    @test (s * a') == (a * s')'
                    @test (a' * s) == (s' * a)'
                    @test (a + b)' == a' + b'
                    @test (a - b)' == a' - b'
                end
            end
            a₀ = randn(Alt(k, n))
            a₁ = randn(Alt(k, n), 2)
            a₂ = randn(Alt(k, n), 2, 2)
            v₁ = randn(2)
            v₂ = randn(2, 2)
            for v in [v₁, v₂]
                for p in [a₀ * v, v * a₀]
                    @test form_size(p) == form_size(a₀)
                    @test size(p) == size(v)
                end
                for p in [a₀ * v', v' * a₀]
                    @test form_size(p) == form_size(a₀)
                    @test size(p) == size(v')
                end
            end
            @test size(v₁' * a₁) == ()
            @test size(v₁ .* a₁) == (2,)
            @test size(a₁ .* v₁) == (2,)
            @test size(v₂ * a₁) == (2,)
            @test size(v₂ * a₂) == (2, 2)
            @test size(v₂ .* a₂) == (2, 2)
            @test size(a₂ .* v₂) == (2, 2)
            @test size(v₁' * a₂) == (1, 2)
            @test size(a₁' * v₂) == (1, 2)
            @test typeof(v₁' * a₂) <: AdjointAltVector
            @test typeof(a₁' * v₂) <: AdjointAltVector
            b₁ = randn(Alt(0, n), 2)
            @test size(a₂ * b₁) == (2,)
            @test size(a₁ .* b₁) == (2,)
        end
    end
end

function test_alt_array_apply()
    for n = 0:4
        for k = 0:n
            for d = 0:3
                _a = randn(ComplexF64, (2:d+1)..., binomial(n, k))
                a = AltArray(k, n, _a)
                v = randn(ComplexF64, n, k)
                r = a(v)
                # test a(v) is alternating
                for p in permutations(1:k)
                    rₚ = a(v[:, p])
                    @test rₚ ≈ r * levicivita(p)
                end
                # test a(v) is linear in each vector
                if k > 0
                    w₁ = randn(ComplexF64, n)
                    w₂ = randn(ComplexF64, n)
                    α₁ = randn(ComplexF64)
                    α₂ = randn(ComplexF64)
                    v₁ = copy(v)
                    v₂ = copy(v)
                    v₃ = copy(v)
                    v₁[:, 1] = w₁
                    v₂[:, 1] = w₂
                    v₃[:, 1] = α₁ * w₁ + α₂ * w₂
                    r₁ = a(v₁)
                    r₂ = a(v₂)
                    r₃ = a(v₃)
                    @test r₃ ≈ α₁ * r₁ + α₂ * r₂
                end
            end
        end
    end
    a = AltVector(2, 3, [1 2 3; 4 5 6])
    v = [1 2; 3 4; 5 6]
    r = a(v)
    @test r == [-16, -40]
end

function test_alt_array_wedge_product()
    for n = 0:4
        for j = 0:n
            a₀ = randn(Alt(j, n))
            a₁ = randn(Alt(j, n), 2)
            a₂ = randn(Alt(j, n), 2, 2)
            A = [a₀, a₁, a₂]
            for k = 0:n-j
                b₀ = randn(Alt(k, n))
                b₁ = randn(Alt(k, n), 2)
                b₂ = randn(Alt(k, n), 2, 2)
                B = [b₀, b₁, b₂]
                for (a, b) in zip(A, B)
                    # test anticommutative
                    c₁ = a .∧ b
                    c₂ = b .∧ a
                    @test c₂ ≈ (-1)^(j * k) * c₁
                    # test linear
                    d = randn(Alt(j, n), size(a)...)
                    e = d .∧ b
                    f = (a + d) .∧ b
                    @test f ≈ c₁ + e
                    # test product shapes
                    @test size(a₀ ∧ b) == size(b)
                    @test size(a ∧ b₀) == size(a)
                    @test size(a₀ ∧ b') == size(b')
                    @test size(a' ∧ b₀) == size(a')
                end
                # test basis-free definition
                v = randn(n, j + k)
                s₁ = (a₂ ∧ b₂)(v)
                s₂ = zeros(2, 2)
                for ρ ∈ Σ(j, j + k)
                    s₂ += sign(ρ) * a₂(v[:, ρ]) * b₂(v[:, ρ'])
                end
                @test s₁ ≈ s₂
                # test output shapes
                @test size(a₁ ∧ b₁') == (2, 2)
                @test size(a₁' ∧ b₁) == ()
                @test size(a₁' .∧ b₁) == (2, 2)
                @test size(a₂ ∧ b₁) == (2,)
                @test size(a₂' ∧ b₁) == (2,)
                @test size(a₁' ∧ b₂) == (1, 2)
                @test size(a₁' ∧ b₂') == (1, 2)
                @test size(a₂ ∧ b₂) == (2, 2)
                @test size(a₂' ∧ b₂) == (2, 2)
                @test size(a₂ ∧ b₂') == (2, 2)
                @test size(a₂' ∧ b₂') == (2, 2)
            end
        end
    end
end

function test_alt_array_pullback()
    for n = 0:4
        for j = 0:n
            a₀ = randn(Alt(j, n))
            a₁ = randn(Alt(j, n), 2)
            a₂ = randn(Alt(j, n), 2, 2)
            A = [a₀, a₁, a₂]
            for k = 0:n-j
                b₀ = randn(Alt(k, n))
                b₁ = randn(Alt(k, n), 2)
                b₂ = randn(Alt(k, n), 2, 2)
                B = [b₀, b₁, b₂]
                for (a, b) in zip(A, B)
                    for d ∈ 0:n+2
                        J = randn(n, d)
                        # test distributive
                        P₁ = J ⋆ (a .∧ b)
                        P₂ = (J ⋆ a) .∧ (J ⋆ b)
                        @test P₁ ≈ P₂
                        # test linearity
                        α = randn()
                        P₃ = J ⋆ (α * (a .∧ b))
                        @test P₃ ≈ α * P₁
                        if j + k ≤ d
                            v = randn(d, j + k)
                            s₁ = P₁(v)
                            s₂ = (a .∧ b)(J * v)
                            @test s₁ ≈ s₂
                        end
                    end
                end
            end
        end
    end
end

function test_alt_array_interior_product()
    for n = 0:4
        u₀ = randn(n)
        u₁ = randn(2, n)
        u₂ = randn(2, 2, n)
        U = [u₀, u₁, u₂]
        for j = 0:n
            a₀ = randn(Alt(j, n))
            a₁ = randn(Alt(j, n), 2)
            a₂ = randn(Alt(j, n), 2, 2)
            A = [a₀, a₁, a₂]
            z₀ = zeros(Alt(j - 2, n))
            z₁ = zeros(Alt(j - 2, n), 2)
            z₂ = zeros(Alt(j - 2, n), 2, 2)
            Z = [z₀, z₁, z₂]
            for (a, z) in zip(A, Z)
                # test right/left is the same when one of the arguments is a scalar
                @test (a ⨼ u₀) == (u₀ ⨽ a)
                # test differential property
                @test (a ⨼ u₀) ⨼ u₀ ≈ z atol = √eps()
            end
            # test Leibniz product rule
            for k = 0:n-j
                b₀ = randn(Alt(k, n))
                b₁ = randn(Alt(k, n), 2)
                b₂ = randn(Alt(k, n), 2, 2)
                B = [b₀, b₁, b₂]
                for (a, b, u) in zip(A, B, U)
                    @test a .⨼ u == u .⨽ a
                    ξ₁ = (a .∧ b) .⨼ u
                    ξ₂ = ((a .⨼ u) .∧ b) + (-1)^j * (a .∧ (b .⨼ u))
                    @test ξ₁ ≈ ξ₂
                end
            end
            # test pullback commutes with interior product
            for d = 0:n+2
                J = randn(n, d)
                if j < d
                    v = randn(d)
                    for a in A
                        @test J ⋆ (a .⨼ (J * v)) ≈ (J ⋆ a) .⨼ v
                    end
                end
            end
            # test output shapes
            for a in A
                @test size(a ⨼ u₀) == size(a)
                @test size(a' ⨼ u₀) == size(a')
            end
            @test typeof(a₁' ⨼ u₀) <: AdjointAltVector
            @test typeof(a₂' ⨼ u₀) <: AdjointAltMatrix
            @test typeof(u₀ ⨽ a₁') <: AdjointAltVector
            @test typeof(u₀ ⨽ a₂') <: AdjointAltMatrix
            @test size(a₁' ⨼ u₁) == ()
            @test size(a₁' ⨼ u₂) == (1, 2)
            @test size(u₂ ⨽ a₁) == (2,)
            @test size(a₂ ⨼ u₁) == (2,)
            @test size(a₂' ⨼ u₁) == (2,)
            @test size(a₂ ⨼ u₂) == (2, 2)
            @test size(a₂' ⨼ u₂) == (2, 2)
        end
    end
end

function test_alt_array_hodge_star()
    for n = 0:4
        for j = 0:n
            for d1 = 0:3
                a = randn(Alt(j, n), collect(2:(d1+1))...)
                b = randn(Alt(j, n), size(a)...)
                c₁ = (a .∧ ★(b)) ⊙ vol(n)
                c₂ = a .⊙ b
                @test c₁ ≈ c₂
                if d1 == 3
                    continue
                end
                for (adj_a, adj_b) in
                    [(false, false), (false, true), (true, false), (true, true)]
                    aop = adj_a ? a' : a
                    sa = (adj_a || d1 != 1) ? size(aop) : (size(aop)..., 1)
                    for d2 = 0:2
                        if d1 == 1 && ((d2 == 1 && adj_a == adj_b) || (d2 == 2 && !adj_a))
                            continue
                        end
                        if d2 == 1 && d1 == 2 && adj_b
                            continue
                        end
                        dims = collect((d1+2):(d1+1+d2))
                        if d1 > 0 && d2 > 0 && (d2 == 2 || !adj_b)
                            match_dim_b = adj_b ? d2 : 1
                            dims[match_dim_b] = sa[end]
                        end
                        b = randn(Alt(j, n), dims...)
                        bop = adj_b ? b' : b
                        c₁ = (aop ∧ ★(bop)) ⊙ vol(n)
                        c₂ = (aop ⊙ bop)
                        @test c₁ ≈ c₂
                    end
                end
                if d1 == 0
                    @test typeof(a ⊙ a) == Float64
                end
                if d1 == 1
                    @test typeof(a' ⊙ a) == Float64
                end
            end
        end
    end
end

function test_alt_array_nary_wedge()
    for n = 0:4
        dx = AltArray(1, n, Matrix{Float64}(I, n, n))
        for k = 0:n
            for ρ ∈ Σ(k, n)
                dxᵨ = dx^ρ
                s = zeros(binomial(n, k))
                s[index(ρ)] = 1.0
                @test dxᵨ == Alt(k, n, s)
            end
        end
    end
end

function test_simplex_constructors()
    for n = 0:4
        for d = 0:n
            for part in integer_partitions(d)
                D = collect(
                    Iterators.flatten([
                        repeat([_v], _a) for
                        (_a, _v) in zip(part, 1.0 .+ rand(length(part)))
                    ]),
                )
                S = collect(Iterators.flatten([[repeat([1], _a - 1); 0] for _a in part]))
                T = Bidiagonal(D, S[1:end-1], :U)
                A = get_random_square_matrix(d, 10.0)
                T = A \ (T * A)
                V = [zeros(1, d); T]
                Q = qr(randn(n, d)).Q[:, 1:d]
                V = V * Q' .+ rand(1, n)
                s = Simplex(V)
                @test abs(s.signed_volume_d_factorial) ≈ prod(D)
                tangents₁ = (Q * qr(T').Q)'
                Q₂ = qr(randn(d, d)).Q[:, :]
                tangents₂ = Q₂ * tangents₁
                tangents₂ = (qr(tangents₂').Q[:, 1:d])'
                if det(tangents₁ * tangents₂') > 0
                    tangents₁[1, :] .*= -1
                end
                s₁ = Simplex(V; tangents = tangents₁, rtol = cond(A) * √eps())
                v₁ = det(s₁)
                @test abs(v₁) ≈ prod(D)
                s₂ = Simplex(V; tangents = tangents₂, rtol = cond(A) * √eps())
                v₂ = det(s₂)
                @test abs(v₂) ≈ prod(D)
                @test sign(v₁) == -sign(v₂)
                @test vertices(s₁, :intrinsic) ≈
                      vertices(s₁, :extrinsic) * tangents(s₁, :extrinsic)'
                @test dλ(s₁) ≈ tangents(s₁, :extrinsic) ⋆ dλ(s₁, :intrinsic)
                @test volume_form(s₁) ≈
                      tangents(s₁, :extrinsic) ⋆ volume_form(s₁, :intrinsic)
            end
        end
    end
end

function test_simplex_show()
    io = IOBuffer()
    s₀₀ = Simplex(zeros(1, 0))
    s₀ = Simplex(Matrix{Float64}(I, 1, 1))
    s₁ = Simplex(Matrix{Float64}(I, 2, 2))
    s₂ = Simplex(Matrix{Float64}(I, 3, 3))
    s₃ = Simplex(Matrix{Float64}(I, 4, 4))
    @test get_show(io, s₀₀, :compact => true) == repr(s₀₀)
    @test get_show(io, s₀₀) == "0-Simplex in ℝ^0 [Simplex{Float64}]"
    @test get_show(io, s₀) == """
0-Simplex in ℝ^1 [Simplex{Float64}]
         x 
  v₁   1.0"""
    @test get_show(io, s₁) == """
1-Simplex in ℝ^2 [Simplex{Float64}]
         x     y 
  v₁   1.0   0.0
  v₂   0.0   1.0"""
    @test get_show(io, s₂) == """
2-Simplex in ℝ^3 [Simplex{Float64}]
         x     y     z 
  v₁   1.0   0.0   0.0
  v₂   0.0   1.0   0.0
  v₃   0.0   0.0   1.0"""
    @test get_show(io, s₃) == """
 3-Simplex in ℝ^4 [Simplex{Float64}]
          x₁     x₂     x₃     x₄ 
   v₁   1.0    0.0    0.0    0.0
   v₂   0.0    1.0    0.0    0.0
   v₃   0.0    0.0    1.0    0.0
   v₄   0.0    0.0    0.0    1.0"""
end

function get_random_simplex(d, n)
    V = randn(d + 1, n)
    V = V .- ones(1, d + 1) * V / (d + 1)
    return Simplex(V)
end

function test_equilateral_simplex()
    for d = 0:4
        l = rand()
        T₁ = equilateral_simplex(d)
        T = equilateral_simplex(d, l)
        V₁ = vertices(T₁)
        V = vertices(T)
        for i = 1:d
            for j = i+1:d
                @test norm(V[i, :] - V[j, :]) ≈ l
                @test norm(V₁[i, :] - V₁[j, :]) ≈ 1
            end
        end
    end
end

function test_right_simplex()
    for d = 0:4
        T = right_simplex(d)
        @test signed_volume(T) ≈ 1 / factorial(d)
    end
end

function test_barycentric_simplex()
    for d = 0:4
        T = barycentric_simplex(d)
        @test dλ(T, :extrinsic) ≈ dλ(T, :barycentric)
        @test volume_form(T, :extrinsic) ≈ volume_form(T, :barycentric)
        @test tangents(T, :extrinsic) ≈ tangents(T, :barycentric)
        @test vertices(T, :extrinsic) ≈ vertices(T, :barycentric)
        @test dim(T) == (d = d, n = d + 1)
    end
end

function test_simplex_hodge_star()
    for d = 0:4
        T_eq = equilateral_simplex(d, √2)
        for n = d:d+2
            T = get_random_simplex(d, n)
            J = get_random_square_matrix(n, 10)
            T₂ = Simplex(vertices(T) * J')
            for k = 0:d
                U = tangents(T)
                ω₀ = randn(Alt(k, d))
                ω = U ⋆ ω₀
                @test form_size(ω) == (k, n)
                μ = ★(T, ω)
                @test form_size(μ) == (d - k, n)
                τ = ★(T, μ)
                @test form_size(τ) == (k, n)
                # test that (★T)^2 == -1^(k*(d-k))
                @test τ ≈ (-1)^(k * (d - k)) * ω
                if n == d
                    # test affine invariance (up to a sign)
                    U₂ = tangents(T₂)
                    η₀ = randn(Alt(k, d))
                    η = U₂ ⋆ η₀
                    @test ★(T, J ⋆ η) ≈ sign(det(J)) * J ⋆ ★(T₂, η)
                    # test that ★T_eq == ★
                    U_eq = tangents(T_eq)
                    η₀ = randn(Alt(k, d))
                    η = U_eq ⋆ η₀
                    @test ★(T_eq, η) ≈ ★(η)
                end
            end
        end
    end
end

function test_pkd_orthogonality()
    for d = 0:4
        for n = 0:d+1
            q, w = simplex_gauss_legendre_quadrature(d, n + 1)
            @test size(q)[end] == d + 1
            @test q * ones(d + 1) ≈ ones(size(q)[1:end-1]...)
            @test sum(w) ≈ √(d + 1) / factorial(d)
            P = pkd(n, q)
            M = P' * Diagonal(w) * P
            @test norm(M - I, Inf) ≈ 0 atol = √eps()
        end
    end
end

function test_trace_free_hodge_star() end

tests = [
    (test_increasing_map, "IncreasingMap"),
    (test_increasing_map_summary, "IncreasingMap summary"),
    (test_alt_array_constructors, "AltArray constructors"),
    (test_alt_array_slicing, "AltArray slicing"),
    (test_alt_array_show, "AltArray show"),
    (test_alt_array_adjoint, "AltArray adjoint"),
    (test_alt_array_linear_algebra, "AltArray linear algebra"),
    (test_alt_array_apply, "AltArray apply"),
    (test_alt_array_wedge_product, "AltArray wedge product"),
    (test_alt_array_pullback, "AltArray pullback"),
    (test_alt_array_interior_product, "AltArray interior product"),
    (test_alt_array_hodge_star, "AltArray hodge star"),
    (test_alt_array_nary_wedge, "AltArray n-ary wedge product"),
    (test_simplex_constructors, "Simplex constructors"),
    (test_simplex_show, "Simplex show"),
    (test_equilateral_simplex, "Equilateral simplex"),
    (test_right_simplex, "Right simplex"),
    (test_barycentric_simplex, "Barycentric simplex"),
    (test_simplex_hodge_star, "Simplex hodge star"),
    (test_pkd_orthogonality, "PKD polynomial orthogonality"),
]

pattern = Regex(get(ENV, "UFSD_TEST_REGEX", ""))

retest = parse(Bool, get(ENV, "UFSD_TEST_RETEST", "false"))

tests = tests[[occursin(pattern, x[2]) for x in tests]]

for (test, name) in tests
    if retest
        @testset "$name (1st)" test()
        @testset "$name (2nd)" test()
    else
        @testset "$name" test()
    end
end
